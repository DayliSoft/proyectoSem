# -*- coding: utf-8 -*-

from django.shortcuts import render
from django.shortcuts import redirect
from django.contrib.auth import authenticate, login
from django.contrib import messages
import hashlib, datetime, random
from django.http import JsonResponse
from django.core.mail import send_mail
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, redirect,get_object_or_404,render_to_response
from django.template.loader import render_to_string
from django.utils import timezone
from django.contrib.auth.decorators import login_required
import os
from django.contrib.auth import logout
import json
from apps.localizacion.models import *
from django.shortcuts import render, redirect, get_object_or_404, render_to_response, RequestContext



def inicio(request):
    if not request.user.is_authenticated():
        return redirect("/")

    cxt = {
    'ubicaciones': loc_localidad.objects.all()
    }
    return render_to_response('localizacion/localizaciones.html', context_instance=RequestContext(request, cxt))

def ubicaciones_select(request):
    datos = []
    ubicaciones = loc_localidad.objects.all().order_by("-loc_id")
    for a in ubicaciones:
        detalle = obtenerPadre(a)
        datos.append({'id': a.loc_id, 'text': a.loc_nombre+detalle})
    return HttpResponse(json.dumps(datos), content_type="application/json")

def obtenerPadre(a):
    detalle = ""
    if a.loc_id_padre_id == 0:
        detalle = ""
    else:
        bandera = False
        while(bandera == False):
            if (a.loc_id_padre_id > 0):
                ubi = loc_localidad.objects.get(loc_id=a.loc_id_padre_id)
                detalle = (detalle)+", "+ubi.loc_nombre
                a = ubi
            else:
                detalle = (detalle)+""
                bandera = True

    return detalle


def agregar_ubicacion(request):
    datos = {}
    nombre = request.POST.get('ubicacion_nombre')
    padre = request.POST.get('id_categoria_padre')
    identificador = request.POST.get('identificador')
    mensaje = "El registro ha sido creado correctamente"
    try:
        if(identificador!="0"):
            #eliminar=loc_localidad.objects.get(loc_id=identificador)
            #eliminar.delete()
            editar = loc_localidad.objects.get(loc_id=int(identificador))
            editar.loc_nombre = nombre
            if padre != '0' or padre is not None:
                editar.loc_id_padre = loc_localidad.objects.get(loc_id=int(padre))

            editar.save()
            messages.add_message(request, messages.SUCCESS, "El registro ha sido actualizado correctamente")
            return redirect("/localidades/index/")
        listado = loc_localidad.objects.all()
        for loc in listado:
            if (loc.loc_nombre == nombre):
                datos['message'] = "No se ha podido ingresar el registro. Verifique que no exista otro con el mismo nombre"
                messages.add_message(request, messages.WARNING, "No se ha podido ingresar el registro. Verifique que no exista otro con el mismo nombre")
                datos['result'] = "X"
                return redirect("/localidades/index/")
        if padre=='0' or padre is None:
            print('padre 0')
            categ = loc_localidad()
            categ.loc_nombre=nombre
        else:
            print(padre)
            categ = loc_localidad(loc_nombre=nombre, loc_id_padre = loc_localidad.objects.get(loc_id=padre))
        messages.add_message(request, messages.SUCCESS, mensaje)
        categ.save()
    except Exception as exc:
        messages.add_message(request, messages.ERROR, "Ha ocurrido un error al tratar de ingresar el registro. COD.: "+str(exc.message))
        datos['message'] = "Ha ocurrido un error al tratar de ingresar el registro. COD.: "+str(exc.message)
        datos['result'] = "X"

    return redirect("/localidades/index/")


def eliminar_ubicacion(request):
    try:
        datos = {}
        ubi = loc_localidad.objects.get(loc_id=request.GET['id_pr'])
        ubi.delete()
        datos['message'] = "Su localidad se ha eliminado"
        messages.add_message(request, messages.SUCCESS, 'Su localidad se ha eliminado')
        datos['result'] = "Ok"
    except Exception as exc:
        messages.add_message(request, messages.SUCCESS, 'Ha ocurrido un error al tratar de eliminar el registro.')
        datos['message'] = "Ha ocurrido un error al tratar de ingresar el registro. COD.: "
        datos['result'] = "X"
    return HttpResponse(json.dumps(datos), content_type="application/json")

def cargar_ubicacione(request):
    try:
        datos = []
        ubi = loc_localidad.objects.get(loc_id=request.GET['padre'])
        datos.append({'id': ubi.loc_id, 'text': ubi.loc_nombre})
        if ubi.loc_id_padre_id > 0:
            print('hay padre', loc_localidad.objects.get(loc_id=ubi.loc_id_padre_id).loc_nombre)
            datos.append({'id_padre': loc_localidad.objects.get(loc_id=ubi.loc_id_padre_id).loc_id})
        else:
            datos.append({'id_padre': 0})
    except Exception as e:
        print(e.message)
        datos.append({'error': e})
    return HttpResponse(json.dumps(datos), content_type="application/json")

def autocopletar_ubicaciones(request):
    datos = {}
    ubicaciones = loc_localidad.objects.all()
    datos["results"] = []
    for a in ubicaciones:
        detalle = obtenerPadre(a)
        datos["results"].append({'id': a.loc_id, 'value': a.loc_nombre, 'info': detalle[2:]})
    return HttpResponse(json.dumps(datos), content_type="application/json")

def guardar_ajax(request):
    datos = {}
    nombre = request.POST.get('ubicacion_nombre')
    padre = request.POST.get('id_categoria_padre')
    mensaje = "El registro ha sido creado correctamente"
    try:
        listado = loc_localidad.objects.all()
        for loc in listado:
            if (loc.loc_nombre == nombre):
                datos['message'] = "No se ha podido ingresar el registro. Verifique que no exista otro con el mismo nombre"
                datos['result'] = "X"
                return HttpResponse(json.dumps(datos), content_type="application/json")

        if padre=='0' or padre is None:
            categ = loc_localidad()
            categ.loc_nombre=nombre
        else:
            categ = loc_localidad(loc_nombre=nombre, loc_id_padre = loc_localidad.objects.get(loc_id=padre))

        categ.save()
        datos['message'] = mensaje
        datos['result'] = "OK"
    except Exception as exc:
        datos['message'] = "Ha ocurrido un error al tratar de ingresar el registro. COD.: "+str(exc.message)
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")