# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sensores', '0002_auto_20170108_1724'),
        ('localizacion', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='loc_area',
            name='loc_id',
        ),
        migrations.RemoveField(
            model_name='loc_calle',
            name='ar_id',
        ),
        migrations.RemoveField(
            model_name='loc_calle',
            name='int_id',
        ),
        migrations.RemoveField(
            model_name='loc_interseccion_dashboard',
            name='loc_id',
        ),
        migrations.RemoveField(
            model_name='loc_semaforo',
            name='sem_calle',
        ),
        migrations.DeleteModel(
            name='loc_tipo_localidad',
        ),
        migrations.DeleteModel(
            name='loc_area',
        ),
        migrations.DeleteModel(
            name='loc_calle',
        ),
        migrations.DeleteModel(
            name='loc_interseccion_dashboard',
        ),
        migrations.DeleteModel(
            name='loc_semaforo',
        ),
    ]
