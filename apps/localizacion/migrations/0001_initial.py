# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import mptt.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='loc_area',
            fields=[
                ('ar_id', models.AutoField(serialize=False, primary_key=True)),
                ('ar_nombre', models.CharField(max_length=100)),
                ('ar_descripcion', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name_plural': 'Areas',
            },
        ),
        migrations.CreateModel(
            name='loc_calle',
            fields=[
                ('cal_id', models.AutoField(serialize=False, primary_key=True)),
                ('cal_nombre', models.CharField(max_length=100)),
                ('cal_principal', models.BooleanField()),
                ('ar_id', models.ForeignKey(to='localizacion.loc_area')),
            ],
            options={
                'verbose_name_plural': 'Calles',
            },
        ),
        migrations.CreateModel(
            name='loc_interseccion_dashboard',
            fields=[
                ('int_id', models.AutoField(serialize=False, primary_key=True)),
                ('int_nombre', models.CharField(max_length=100)),
                ('int_descripcion', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name_plural': 'Intersecciones',
            },
        ),
        migrations.CreateModel(
            name='loc_localidad',
            fields=[
                ('loc_id', models.AutoField(serialize=False, primary_key=True)),
                ('loc_nombre', models.CharField(max_length=50)),
                ('lft', models.PositiveIntegerField(editable=False, db_index=True)),
                ('rght', models.PositiveIntegerField(editable=False, db_index=True)),
                ('tree_id', models.PositiveIntegerField(editable=False, db_index=True)),
                ('level', models.PositiveIntegerField(editable=False, db_index=True)),
                ('loc_id_padre', mptt.fields.TreeForeignKey(related_name='children', blank=True, to='localizacion.loc_localidad', null=True)),
            ],
            options={
                'verbose_name_plural': 'Localidades',
            },
        ),
        migrations.CreateModel(
            name='loc_semaforo',
            fields=[
                ('sem_id', models.AutoField(serialize=False, primary_key=True)),
                ('sem_ubicacion', models.CharField(max_length=100)),
                ('sem_tiempo_cambio', models.IntegerField()),
                ('sem_calle', models.ForeignKey(to='localizacion.loc_calle')),
            ],
            options={
                'verbose_name_plural': 'Semaforos',
            },
        ),
        migrations.CreateModel(
            name='loc_tipo_localidad',
            fields=[
                ('tloc_id', models.AutoField(serialize=False, verbose_name=b'Id', primary_key=True)),
                ('tloc_nombre', models.CharField(max_length=200, verbose_name=b'Nombre')),
            ],
            options={
                'verbose_name_plural': 'TipoLocalidades',
            },
        ),
        migrations.AddField(
            model_name='loc_interseccion_dashboard',
            name='loc_id',
            field=models.ForeignKey(to='localizacion.loc_localidad'),
        ),
        migrations.AddField(
            model_name='loc_calle',
            name='int_id',
            field=models.ForeignKey(to='localizacion.loc_interseccion_dashboard'),
        ),
        migrations.AddField(
            model_name='loc_area',
            name='loc_id',
            field=models.ForeignKey(to='localizacion.loc_localidad'),
        ),
    ]
