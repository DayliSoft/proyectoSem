
from django.db import models
from django.conf import settings
import datetime
from mptt.models import MPTTModel, TreeForeignKey
# from apps.geolocalizacion.models import geo_mapa

class loc_localidad(MPTTModel):
    loc_id = models.AutoField(primary_key=True)
    loc_nombre = models.CharField(max_length=50)
    loc_id_padre = TreeForeignKey('self', null=True, blank=True, related_name='children', db_index=True)

    def __unicode__(self):
        return self.loc_nombre

    class MPTTMeta:
        parent_attr = 'loc_id_padre'
        order_insertion_by = ['loc_nombre']

    class Meta:
        verbose_name_plural='Localidades'
        app_label = 'localizacion'


