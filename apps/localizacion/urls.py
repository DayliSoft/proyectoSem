from django.conf.urls import include, url
from django.contrib import admin
from apps.localizacion.views import *

urlpatterns = [
    url(r'^index', inicio, name="inicio_loc"),
    url(r'^ubi_select', ubicaciones_select, name="cargar_ubicaciones_select"),
    url(r'^guardarUbicacion', agregar_ubicacion, name="cargar_ubicaciones_select"),
    url(r'^eliminarUbicacion', eliminar_ubicacion, name="url_eleminar_ubicacion"),
    url(r'^verDatosUbicaciones', cargar_ubicacione, name="url_ver_ubicacion"),

    url(r'^autocompletar', autocopletar_ubicaciones, name="autocompletar"),
    url(r'^guardar_ajax', guardar_ajax, name="guardar_ajax"),
]
