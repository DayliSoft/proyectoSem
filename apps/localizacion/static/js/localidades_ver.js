
$().ready(function(){
    var clases = $(this);
    activarDiv();
    cargar_ubicaciones_select("id_categoria_padre");
});
function agregar(){
    $("#myModalLabel").text("Agregar Localidad");
    if($("#btnGuardarYAdd").length == 0)
        $('<button id="btnGuardarYAdd" onclick="guardarYAddOtro()" class="btn btn-primary" type="button"><i class="fa fa-check-square-o"></i> Guardar y agregar otra</button>').insertBefore("#btnGuardar");

    //$('#id_categoria_padre').select2().enable(false);
    $('#modalAgregar').modal({backdrop: "static", keyboard: false});

    document.getElementById("formUbicacion").reset();
    cargar_ubicaciones_select("id_categoria_padre")
}
function activarDiv(){
    $('#dashboard').removeClass('nav-active');
    $('#mapas').removeClass('nav-active');
    $('#sensores').removeClass('nav-expanded');
    $('#sensor').removeClass('nav-active');
    $('#dispositivo').removeClass('nav-active');
    $('#localidades').addClass('nav-active');
}

function cambiar(argument) {
 if($("#switch").is(':checked') ) {
         // alert('Seleccionado');
         // Prenderse el select
         $('#ocultar_select').addClass('hide');
     }else{
        $('#ocultar_select').removeClass('hide');
    }
}
    /*
    Bootstrap Confirmation - CALLBACK
    */
    function eliminar(){
        if($('#treeBasic').find('a.jstree-clicked').attr('href') || validarBusquedaUbicaciones()){
            $('#modalEliminar').modal({backdrop: 'static', keyboard: false});
        }else{
            toastr.error('Seleccione una localidad', 'Info');
            $("#txtAutocompletar").val("");
            $("#txtAutocompletar").focus();
        }
    }

    function eliminarObjeto(){
        datos = {'id_pr': $("#menu_localidades").data("test").localidad_id};
        if(datos.id_pr == "")
            datos = {'id_pr': ($('#treeBasic').find('a.jstree-clicked').attr('href'))};

        $.ajax({
            type: 'get',
            url: "/localidades/eliminarUbicacion/",
            data: datos,
            success: function (json) {
                location.href = "/localidades/index/"
            }
        });
    }

    function editar(){
        var valorArbol = $('#treeBasic').find('a.jstree-clicked').attr('href');
        if(valorArbol || validarBusquedaUbicaciones()){
            $('select').select2();

            if(valorArbol != null)
                $("#menu_localidades").data("test", {localidad_id: Number(valorArbol)});

            datos = {'padre': $("#menu_localidades").data("test").localidad_id};
            if(datos.padre == "")
                datos = {'padre':($('#treeBasic').find('a.jstree-clicked').attr('href'))};

            $.ajax({
                type: 'get',
                url: "/localidades/verDatosUbicaciones/",
                data: datos,
                success: function(json){
                    $('#ubicacion_nombre').val(json[0].text);
                    $('#identificador').val(json[0].id);
                    // $("#id_categoria_padre").val(json[0].id).prop('disabled','disabled');
                    $("#id_categoria_padre").find('option[value=' + json[0].id + ']').attr('disabled', 'disabled');
                    $('select').select2();
                    if(json[1].id_padre==0){
                        $('#check').find('div.ios-switch').removeClass('off');
                        $('#check').find('div.ios-switch').addClass('on');
                        $('#ocultar_select').addClass('hide');
                    }else{
                        $('#check').find('div.ios-switch').removeClass('on');
                        $('#check').find('div.ios-switch').addClass('off');
                        $('#ocultar_select').removeClass('hide');

                        $("#id_categoria_padre").val(json[1].id_padre).trigger("change");
                    }
                }
            });
            $("#myModalLabel").text("Editar Localidad");
            $("#btnGuardarYAdd").remove();
            $('#modalAgregar').modal({backdrop: 'static', keyboard: false});
        }else{
            toastr.error('Seleccione una localidad', 'Info');
            $("#txtAutocompletar").val("");
            $("#txtAutocompletar").focus();
        }
    }

function validarBusquedaUbicaciones() {
    var id = $("#menu_localidades").data("test").localidad_id;
    if($("#txtAutocompletar").val().trim() == "" || id == "")
        return false;
    else
        return true;
}

function resetArbolInput() {
    if($("#modalAgregar").data("test").m != ""){
        location.reload();
    }else{
        //Habilitar el select antes de abril modal editar
        var idData = $("#menu_localidades").data("test").localidad_id;
        if (idData != ""){
            $("#id_categoria_padre").find('option[value=' + idData +']').attr('disabled', false);
            $('select').select2();
        }

        $("#menu_localidades").data("test", {localidad_id: ""});
        $("#txtAutocompletar").val("");
        $('#treeBasic').find('a.jstree-clicked').removeClass('jstree-clicked');
        $('#modalAgregar').modal('hide');
        $('#modalEliminar').modal('hide');
    }
}

function guardarYAddOtro() {
    if($("#ubicacion_nombre").val().trim() != "") {
        var form = document.forms.namedItem("formUbicacion");
        datos = new FormData(form);
        $('<img id="img_ajax_guardar" src="../../static/assets/images/ajax_load.gif">').insertBefore("#btnGuardarYAdd");

        $.ajax({
            type: "POST",
            url: "/localidades/guardar_ajax/",
            dataType: "json",
            async: true,
            data: datos,
            processData: false,
            contentType: false,
            success: function (json) {
                $("#img_ajax_guardar").remove();
                if (json.result == "OK") {
                    toastr.options = {"progressBar": true};
                    toastr.success(json.message, 'Info');
                    $("#modalAgregar").data("test", {m: "OK"});
                    if ($("#id_categoria_padre").length == 0) {
                        $("#ocultar_select").load(location.href + " #ocultar_select>*", "");
                    }

                    cargar_ubicaciones_select("id_categoria_padre");
                    $("#ubicacion_nombre").val("");
                    $("#ubicacion_nombre").focus();
                } else {
                    toastr.error('Ha ocurrido un error: ' + json.message, 'Info');
                }
            },
            error: function (e) {
                $("#img_ajax_guardar").remove();
                toastr.error("Lo sentimos", "Ha ocurrido un error, inténtelo más tarde.", 'Info');
            }
        });

        $("#id_categoria_padre").select2({
            allowClear: true
        });
    }else{
        toastr.error('Escriba un nombre', 'Info');
    }
}