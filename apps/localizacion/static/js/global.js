// Cargar combo de ubicaciones
function cargar_ubicaciones_select(nombre_id){
    $.ajax({
        url:"/localidades/ubi_select/",
        dataType: "json",
        async: true,
        success: function(json){
            $("#"+nombre_id).select2({
                placeholder: 'Seleccione una Dependencia',
                data: json,
                allowClear: true
            });
        }   
    });
}

// Automcomplete
function iniciarAutocomplete(nombre_id_input, nombre_id_menu) {
    var options = {
        script: "/localidades/autocompletar/",
        varname: nombre_id_input,
        json:true,
        noresults: "¡Sin resultados!",
        callback: function (obj) {
            $("#"+nombre_id_menu).data("test", {localidad_id: obj.id});
            $('#treeBasic').find('a.jstree-clicked').removeClass('jstree-clicked');
        }
    };
    var as_json = new AutoSuggest(nombre_id_input, options);
}

// Modal global agregar dispositivo
function abrirModalAgregarDispositivo() {
    document.getElementById("gloFormModalDispositivo").reset();
    $("#gloModalDispositivo").modal({
        backdrop: 'static',
        keyboard: false
    })
}

//Cargar objetos en selects (select2)
function cargar_objetos_select(id, url){
     $.ajax({
         url: url,
         dataType: "json",
         async: true,
         success: function(json){
             if(json.length > 0){
                id_f = json[0].id;
                 $("#"+id).select2({
                     data: json,
                     allowClear: true
                 });

                 id_ff = id_f;

                 $("#"+id).val(id_ff);

                 $("#"+id).select2({
                     selected: id_ff
                 });
             }else{
                 $("#"+id).select2({
                     allowClear: true
                 });
             }
         }
    });
}

//Guardar dispositivo modal global
function guardarDispositivoModal() {
    if($("#gloFormModalDispositivo").valid()){
        var form = document.forms.namedItem("gloFormModalDispositivo");
        datos = new FormData(form);
        $('<img id="img_ajax_guardar" src="../../static/assets/images/ajax_load.gif">').insertBefore("#btnAgregarDispositivoModal");

        $.ajax({
            type: "POST",
            url: "/maps/guardarDispositivoModal/",
            dataType: "json",
            async: true,
            data: datos,
            processData: false,
            contentType: false,
            success: function (json) {
                $("#img_ajax_guardar").remove();
                if (json.result == "OK") {
                    toastr.success(json.message, 'Correcto');    
                    $("#gloModalDispositivo").modal("hide");
    
                    //Actualizar select en modal agregar semaforo
                    cargar_objetos_select("select_dispositivo", "/maps/cargarDispositivosSelect/");

                    //Actualizar select en modal agregar sensor
                    cargar_objetos_select("select_dispositivo_sen", "/maps/cargarDispositivosSelect/");
                } else {
                    toastr.error(json.message, 'Estado');
                }
            },
            error: function(e) {
                $("#img_ajax_guardar").remove();
                toastr.error("Ha ocurrido un error, inténtelo más tarde.", 'Lo sentimos');
                console.log(e);
            }
        });
    }else{
        toastr.warning("Revise que los campos no estén vacíos", 'Estado');
    }    
}

//Guardar dependencia modal global
function guardarDependenciaModal() {
    if($("#txt_ubicacion_modal").val().trim() != ""){
        var form = document.forms.namedItem("gloFormModalDependencia");
        datos = new FormData(form);
        $('<img id="img_ajax_guardar" src="../../static/assets/images/ajax_load.gif">').insertBefore("#btnAgregarDependenciaModal");

        $.ajax({
            type: "POST",
            url: "/maps/guardarDependenciaModal/",
            dataType: "json",
            async: true,
            data: datos,
            processData: false,
            contentType: false,
            success: function (json) {
                $("#img_ajax_guardar").remove();
                if (json.result == "OK") {
                    toastr.success(json.message, 'Correcto');
                    $("#gloModalDependencia").modal("hide");

                    //Actualizar select en modal agregar semaforo
                    cargar_objetos_select("id_categoria_padre_mapa", "/localidades/ubi_select/");
                    cargar_objetos_select("select_dependencia_modal", "/localidades/ubi_select/");
                } else {
                    toastr.error(json.message, 'Estado');
                }
            },
            error: function(e) {
                $("#img_ajax_guardar").remove();
                toastr.error("Ha ocurrido un error, inténtelo más tarde.", 'Lo sentimos');
                console.log(e);
            }
        });
    }else{
        toastr.warning("Revise que los campos no estén vacíos", 'Estado');
    }
}

/* Agregar calles e intersecciones */
function resetFormCallesIntersecciones(id_modal, id_input) {
    if($("#"+id_modal).data("test").m != ""){
        location.reload();
    }else{
        $("#"+id_input).val("");
        $('#'+id_modal).modal('hide');
    }
}

function guardarYAddOtraCalleInter(id_input, formulario, boton_add, url, id_modal) {
    //if($("#"+id_input).val().trim() != "") {
    if($("#"+formulario).valid()) {
        var form = document.forms.namedItem(formulario);
        datos = new FormData(form);
        $('<img id="img_ajax_guardar" src="../../static/assets/images/ajax_load.gif">').insertBefore("#"+boton_add);

        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            async: true,
            data: datos,
            processData: false,
            contentType: false,
            success: function (json) {
                $("#img_ajax_guardar").remove();
                if (json.result == "OK") {
                    toastr.options = {"progressBar": true};
                    toastr.success(json.message, 'Info');

                    $("#"+id_modal).data("test", {m: "OK"});
                    document.getElementById(formulario).reset();

                    var t = $("#"+formulario).find('select');
                    for(var i = 0; i < t.length; i++){
                        $("#"+t[i].attributes[1].value).select2({
                            selected: "0"
                        });
                    }

                    $("#"+id_input).focus();
                } else {
                    toastr.error('Ha ocurrido un error: ' + json.message, 'Info');
                }
            },
            error: function (e) {
                console.log(e);
                $("#img_ajax_guardar").remove();
                toastr.error("Lo sentimos", "Ha ocurrido un error, inténtelo más tarde.", 'Info');
            }
        });
    }else{
        toastr.error('Los campos no deben estar vacíos', 'Info');
    }
}

//Selectize inicializar
function cargar_categorias(){
    $('#input-tags').selectize({
        plugins: ['drag_drop'],
        delimiter: ',',
        maxItems:1,
        persist: true,
        create: function(input) {
            return {
                value: input,
                text: input
            }
        }
  });
     $('#input-tags1').selectize({
      plugins: ['drag_drop'],
      delimiter: ',',
      maxItems:1,
      persist: true,
      create: function(input) {
          return {
              value: input,
              text: input
          }
      }
  });
}

function editarSensorModal(id_sensor, modo) {
    if($("#formularioCalles").valid()) {
        var form = document.forms.namedItem("formularioCalles");
        datos = new FormData(form);
        datos.append('id', id_sensor);
        
        $('<img id="img_ajax_guardar" src="../../static/assets/images/ajax_load.gif">').insertBefore("#btnHideSensor");

        $.ajax({
            type: "POST",
            url: "/maps/editarSensor/",
            dataType: "json",
            async: true,
            data: datos,
            processData: false,
            contentType: false,
            success: function (json) {
                $("#img_ajax_guardar").remove();
                if (json.result == "OK") {
                    $("#modalCalles1").modal("hide");
                    toastr.options = {"progressBar": true};
                    toastr.success(json.message, 'Info');
                    if (modo==0){
                        $("#datatable-default").load(location.href+" #datatable-default>*","");

                    }else{
                        cargarMapa(json.map);
                    }
                } else {
                    toastr.error('Ha ocurrido un error: ' + json.message, 'Info');
                }
            },
            error: function (e) {
                console.log(e);
                $("#img_ajax_guardar").remove();
                toastr.error("Lo sentimos", "Ha ocurrido un error, inténtelo más tarde.", 'Info');
            }
        });
    }else{
        toastr.error('Los campos no deben estar vacíos', 'Info');
    }
}