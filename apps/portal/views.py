from django.shortcuts import render, render_to_response, RequestContext, HttpResponse,redirect
from apps.sensores.models import *
from apps.geolocalizacion.models import *
import json
from django.views.decorators.csrf import csrf_exempt
# Create your views here.
def pag_principal(request):
    if request.user.is_authenticated():
        itersecciones = geo_interseccion_dashboard.objects.all()
        dic = {
            'int' : itersecciones,
        }
        return render_to_response('portal/index.html', dic, context_instance=RequestContext(request))
    else:
        return redirect("/users/login/")

def dashboard(request, id):
    datosSensores = []
    findIntersecciones = sen_calle_interseccion_dashboard_sensor.objects.filter(scint_intereseccion = id).order_by('scint_calle_mapa')
    if findIntersecciones.exists():
        print findIntersecciones
        dic = {
            'sensores' : findIntersecciones,
            'id_interseccion' : id
        }

        return render_to_response('portal/dashboard.html', dic, context_instance=RequestContext(request))
    # print datosSensores
    return render_to_response('portal/dashboard.html', context_instance=RequestContext(request))

def lista(request):
    return render(request, 'portal/lista_intersecciones.html')

@csrf_exempt
def datos_interseccion(request):
    print request.POST
    try:
        respuesta = []
        idInterseccion = request.POST['id_interseccion']
        findSensoresIntersecciones = sen_calle_interseccion_dashboard_sensor.objects.filter(scint_intereseccion = idInterseccion, scint_sensor__sact_tipo='ultrasonico')
        if findSensoresIntersecciones.exists():
            for sensores in findSensoresIntersecciones:
                print len(findSensoresIntersecciones)
                findSemaforo = sen_calle_interseccion_dashboard_sensor.objects.get(scint_calle_mapa__cmap_calle = sensores.scint_calle_mapa.cmap_calle,scint_sensor__sact_tipo='semaforo')
                respuesta.append({
                    'id_sensor' : sensores.scint_sensor.id_sensor,
                    'id_calle' : sensores.scint_calle_mapa.cmap_calle.pk,
                    'id_interseccion' : idInterseccion,
                    'id_semaforo' : findSemaforo.scint_sensor.id_sensor
                })
        res = {'result': 'OK', 'data': respuesta}
    except Exception as e:
        print e
        res = {'result': 'ERROR', 'data': None}
        raise e
    return HttpResponse(json.dumps(res))

@csrf_exempt
def getAllintersecciones(request):
    try:
        respuesta = []
        intersecciones = geo_interseccion_dashboard.objects.all().values('pk')
        for id in intersecciones:
            respuesta.append(id['pk'])
        res = {'result': 'OK', 'data': respuesta}
    except Exception as e:
        res = {'result': 'ERROR', 'data': None}
        print e
    return HttpResponse(json.dumps(res))

@csrf_exempt
def setCalleSemaforoPrincipal(request):

    try:
        print request.POST
        calle = geo_calle.objects.get(pk = request.POST['id_calle'])
        calle.cal_principal = True
        calle.save()
        semaforo = sen_semaforo.objects.get(sem_id__id_sensor=request.POST['id_semaforo'])
        semaforo.sem_tiempo_cambio = 6
        semaforo.save()
        res = {'result': 'OK'}
    except Exception as e:
        res = {'result': 'ERROR'}
        print e
    return HttpResponse(json.dumps(res))

@csrf_exempt
def setCalleSemaforoSecundario(request):

    try:
        print request.POST
        calle = geo_calle.objects.get(pk = request.POST['id_calle'])
        calle.cal_principal = False
        calle.save()
        semaforo = sen_semaforo.objects.get(sem_id__id_sensor=request.POST['id_semaforo'])
        semaforo.sem_tiempo_cambio = 3
        semaforo.save()
        res = {'result': 'OK'}
    except Exception as e:
        res = {'result': 'ERROR'}
        print e
    return HttpResponse(json.dumps(res))