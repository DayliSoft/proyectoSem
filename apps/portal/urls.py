from django.conf.urls import include, url
from django.contrib import admin
from apps.portal.views import *

urlpatterns = [
    url(r'^index', pag_principal,name="index"),
	url(r'^dashboard/(\d+)/$', dashboard),
    url(r'^lista', lista,name="lista"),
    url(r'^datos_interseccion$',datos_interseccion),
    url(r'^getAllintersecciones$',getAllintersecciones),
    url(r'^setCalleSemaforoPrincipal$',setCalleSemaforoPrincipal),
    url(r'^setCalleSemaforoSecundario$',setCalleSemaforoSecundario),
]
