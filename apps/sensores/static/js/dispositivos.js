
$().ready(function(){
   cargar_mapas();
   activarDiv();

});

function agregar(){
    $("#modalLabelDispositivo").text("Agregar Dispositivo");
    if($("#btnGuardarYAddDispositivo").length == 0){
        parametros = "'txt_nombre', 'formDispositivo', 'btnGuardarYAddDispositivo', '/sensores/m/guardarDispositivoModal/', 'modalAgregar'";
        $('<button id="btnGuardarYAddDispositivo" onclick="guardarYAddOtraCalleInter('+parametros+')" class="btn btn-primary" type="button"><i class="fa fa-check-square-o"></i> Guardar y agregar otro</button>').insertBefore("#btnGuardarInterseccion");
    }

    $('#modalAgregar').modal({backdrop: "static", keyboard: false});
    document.getElementById("formDispositivo").reset();
}

function activarDiv(){
    $('#datatable-default_filter').find('input').addClass('form-control');
    $('#dashboard').removeClass('nav-active');
    $('#mapas').removeClass('nav-active');
    $('#sensores').removeClass('nav-expanded');
    $('#sensor').removeClass('nav-active');
    $('#localidades').removeClass('nav-active');

    $('#sensores').addClass('nav-expanded');
    $('#sensores').addClass('nav-active');
    $('#dispositivo').addClass('nav-active');

}
function cargar_mapas(){
    $('#datatable-default').dataTable();
    $("#id_categoria_padre").select2({
        allowClear: true
    });
}

function eliminarObjeto(id){
    $.ajax({
        type: 'get',
        url: "/localidades/eliminarDispositivo/",
        data: {'pk':id},
        success: function(json){
            location.href="/sensores/index_d/"
        }
    });
}
function eliminar(id){
    $("#txt_id").val(id);
    $('#modalEliminar').modal({
        show: 'true'
    });

}
function editar(id){
    $('select').select2();
    $.ajax({
        type: 'get',
        url: "/sensores/verDatosDispositivos/",
        data: {'pk':id},
        success: function(json){
            $('#txt_nombre').val(json[0].nombre);
            $('#txt_descripcion').val(json[0].descripcion);
            $('#txt_loc').val(json[0].localidad);
            $('#txt_mac').val(json[0].mac);
            $('#identificador').val(json[0].id);
            $('select').select2();
            $("#id_categoria_padre2").val(json[0].mapa).trigger("change");
        }
    });
    $("#modalLabelDispositivo").text("Editar Dispositivo");
    $("#btnGuardarYAddDispositivo").remove();
    $('#modalAgregar').modal({backdrop: 'static', keyboard: false});
}

