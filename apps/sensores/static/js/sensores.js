
$().ready(function(){
   cargar_mapas();
   activarDiv();

});

function activarDiv(){
    $('#datatable-default_filter').find('input').addClass('form-control');
    $('#dashboard').removeClass('nav-active');
    $('#mapas').removeClass('nav-active');
    $('#dispositivo').removeClass('nav-active');
    $('#localidades').removeClass('nav-active');
    $('#dispositivo').removeClass('nav-active');

    $('#sensores').addClass('nav-expanded');
    $('#sensores').addClass('nav-active');
    $('#sensor').addClass('nav-active');
}
function cargar_mapas(){
    $('#datatable-default').dataTable();
    $("#id_categoria_padre1").select2({
        allowClear: true
    });
    $("#id_categoria_padre2").select2({
        allowClear: true
    });
    $("#txt_tipo").select2({minimumResultsForSearch: -1});
}
function eliminar(id){
    $("#txt_id").val(id);
    $('#modalEliminar').modal({
        show: 'true'
    });

}
function editar(id){
    $.ajax({
        type: 'get',
        url: "/maps/getSensor/",
        data: {'id':id},
        success: function(data){
            console.log(data);
            $("#txt_semaforo_nombre").val(data[0].nombre);
            $("#txt_id_sensor").val(data[0].sensor_id);

            var $select = $('#input-tags').selectize();
            var calles = $select[0].selectize;
            calles.addItem(data[0].calle);

            var $select2 = $('#input-tags1').selectize();
            var intersec = $select2[0].selectize;
            intersec.addItem(data[0].interseccion);


            if(data[0].principal==true){
                $('#check').find('div.ios-switch').removeClass('off');
                $('#check').find('div.ios-switch').addClass('on');
            }else{
                $('#check').find('div.ios-switch').removeClass('on');
                $('#check').find('div.ios-switch').addClass('off');
            }

            $("#select_dispositivo").val(data[0].dispositivo).trigger("change");
            $("#select_tipo").val(data[0].tipo).trigger("change");

            $("#btnEliminar").remove();
            $("#btnAgregarSemaforo").remove();
            $('<button type="button" id="btnAgregarSemaforo" onclick="editarSensorModal('+data[0].id+',0)" class="btn btn-primary">Editar</button>').insertAfter("#btnHideSensor");

            $("#gridSystemModalLabel").text("Editar Sensor");

            $("#modalCalles1").modal({
                backdrop: 'static',
                keyboard: false
            });
        }
    });
}


function configurar(id){
    $('#txt_id_sensor').val(id);
    $('#modalConfigurar').modal({
        show: 'true'
    });
}



function configurarVerDato(id, dispositivo){
    $('#txt_id_sensor').val(id)
    $("#dispositivos").val(dispositivo).trigger("change");
    $('#modalConfigurar').modal({
        show: 'true'
    });
}