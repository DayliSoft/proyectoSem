from django.conf.urls import include, url
from django.contrib import admin
from apps.sensores.views import *

urlpatterns = [
    #DISPOSITIVOS
    url(r'^index_d', inicioD, name="inicio_dispositivos"),
    url(r'^verDatosDispositivos', cargarDispositivo, name="cargar_dispositivo"),
    url(r'^guardarDispositivo', guardarDispositivo, name="guardarDispositivo"),
    url(r'^eliminarDispositivo', eliminarDispositivo, name="eliminarDispositivo"),
    url(r'^cargarMapas', cargarMapas, name="cargarMapas"),
    #SENSORES
    url(r'^cargarSelects', cargarSelects, name="cargarSelects"),
    url(r'^verDatosSensores', cargarSensor, name="cargarSensor"),
    url(r'^eliminarSensor', eliminarSensor, name="eliminarSensor"),
    url(r'^guardarSensor', guardarSensor, name="guardarSensor"),
    url(r'^configurarDipositivoSensor', configurarSensor, name="configurarSensor"),
    url(r'^index_s', inicioS, name="inicio_sensores"),

    url(r'^m/guardarDispositivoModal/', guardarDispositivoAjax, name="guardarDispositivoAjax"),


]
