# -*- coding: utf-8 -*-

from django.shortcuts import render
from django.shortcuts import redirect
from django.contrib.auth import authenticate, login
from django.contrib import messages
import hashlib, datetime, random
from django.http import JsonResponse
from django.core.mail import send_mail
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, redirect,get_object_or_404,render_to_response
from django.template.loader import render_to_string
from django.utils import timezone
from django.contrib.auth.decorators import login_required
import os
from django.contrib.auth import logout
import json
from apps.localizacion.models import *
from apps.sensores.models import *
from django.shortcuts import render, redirect, get_object_or_404, render_to_response, RequestContext



########################################################
####################DISPOSITIVOS########################
########################################################
def inicioD(request):
    if not request.user.is_authenticated():
        return redirect("/")

    cxt = {
    'mapas': geo_mapa.objects.all(),
    'dispositivos': sen_dispositivo.objects.all()
    }
    return render_to_response('sensores/indexDispositivos.html', context_instance=RequestContext(request, cxt))
def cargarMapas(request):
    datos = []
    mapas = geo_mapa.objects.all()
    for a in mapas:
        datos.append({'id': a.map_id, 'text': a.map.map_nombre})
    return HttpResponse(json.dumps(datos), content_type="application/json")

def guardarDispositivo(request):
    nombre = request.POST.get('txt_nombre')
    descripcion = request.POST.get('txt_descripcion')
    mac = request.POST.get('txt_mac')
    identificador = request.POST.get('identificador')
    localidad = request.POST.get('txt_loc')

    try:
        if(identificador!="0"):
            dispositivo=sen_dispositivo.objects.get(dis_id=identificador)
            repetidos = sen_dispositivo.objects.all().exclude(dis_id=identificador)
            bandera = True
            msj = "El registro se actualizo"

            for r in repetidos:
                if r.dis_nombre.lower() == str(nombre).lower():
                    bandera = False
                    msj = "Ya existe un dispositivo llamado " + nombre
                    break

                if r.dis_mac.lower() == str(mac).lower():
                    bandera = False
                    msj = "Ya existe un dispositivo con la MAC " + mac
                    break

            if bandera:
                dispositivo.dis_nombre=nombre
                dispositivo.dis_localizacion=localidad
                dispositivo.dis_descripcion=descripcion
                dispositivo.dis_mac=mac
                dispositivo.save()
                #poner codigo para actualizar
                messages.add_message(request, messages.SUCCESS, msj)
            else:
                messages.add_message(request, messages.ERROR, msj)
        else:
            repetidos = sen_dispositivo.objects.all()
            bandera = True
            msj = "Se creo un nuevo dispositivo"

            for r in repetidos:
                if r.dis_nombre.lower() == str(nombre).lower():
                    bandera = False
                    msj = "Ya existe un dispositivo llamado " + nombre
                    break

                if r.dis_mac.lower() == str(mac).lower():
                    bandera = False
                    msj = "Ya existe un dispositivo con la MAC " + mac
                    break

            if bandera:
                dispositivo = sen_dispositivo(dis_nombre=nombre, dis_descripcion=descripcion, dis_mac=mac, dis_localizacion=localidad)
                dispositivo.save()
                messages.add_message(request, messages.SUCCESS, msj)
            else:
                messages.add_message(request, messages.ERROR, msj)
    except Exception as exc:
        messages.add_message(request, messages.ERROR, "Ha ocurrido un error al tratar de ingresar el registro. COD.: "+str(exc.message))

    return redirect("/sensores/index_d/")
def cargarDispositivo(request):
    try:
        datos = []
        dispositivo = sen_dispositivo.objects.get(dis_id=request.GET['pk'])
        datos.append({'id': dispositivo.dis_id, 'nombre': dispositivo.dis_nombre,
                        'descripcion':dispositivo.dis_descripcion, 'localidad':dispositivo.dis_localizacion,
                        'mac':dispositivo.dis_mac})
    except Exception as e:
        print(e.message)
        datos.append({'error': e})
    return HttpResponse(json.dumps(datos), content_type="application/json")
def eliminarDispositivo(request):
    datos = {}
    id_dis = request.POST.get('txt_id')
    try:
        dispositivo=sen_dispositivo.objects.get(dis_id=id_dis)
        dispositivo.delete()
        messages.add_message(request, messages.SUCCESS, "Se elimino el dispositivo")
    except Exception as exc:
        messages.add_message(request, messages.ERROR, "Ha ocurrido un error al tratar de ingresar el registro. COD.: "+str(exc.message))

    return redirect("/sensores/index_d/")

########################################################
######################SENSORES##########################
########################################################

def inicioS(request):
    if not request.user.is_authenticated():
        return redirect("/")

    calles = ""
    for i in geo_calle.objects.all():
        calles = calles + ',' + str(i.cal_nombre)
    intersecciones = ""
    for i in geo_interseccion_dashboard.objects.all():
        intersecciones = intersecciones + ',' + str(i.int_nombre)

    dispositivos = sen_dispositivo.objects.all().order_by("-dis_id")


    cxt = {
    'calles': calles,
    'intersecciones': intersecciones,
    'dispositivos': sen_dispositivo.objects.all(),
    'sensores': sen_sensor_actuador.objects.all()
    }
    return render_to_response('sensores/indexSensores.html', context_instance=RequestContext(request, cxt))

def cargarSelects(request):
    datos = []
    intersecciones = geo_interseccion_dashboard.objects.all()
    #datos[0] cuantas intersecciones hay
    datos.append({'numIntersecciones': intersecciones.count()})
    dispositivos=sen_dispositivo.objects.all();
    datos.append({'numDispositivos': dispositivos.count()})
    for a in intersecciones:
        datos.append({'id': a.int_id, 'text': a.int_nombre})
    for a in dispositivos:
        datos.append({'id': a.dis_id, 'text': a.dis_nombre})
    return HttpResponse(json.dumps(datos), content_type="application/json")


def guardarSensor(request):
    datos = {}
    nombre = request.POST.get('txt_nombre')
    tipo = request.POST.get('txt_tipo')
    identificador = request.POST.get('identificador')
    id_sensor = request.POST.get('txt_id_sensor')
    dispositivo = request.POST.get('select_dispositivo_sen')
    latitud = request.POST.get('txt_latitud_sensor')
    longitud = request.POST.get('txt_longitud_sensor')

    if int(dispositivo) == 0:
        messages.add_message(request, messages.ERROR,"Debe elegir un dispositivo")
        return redirect("/sensores/index_s/")

    try:
        msj = "El registro se actualizo"
        todos = sen_sensor_actuador.objects.all()
        bandera = True

        if(identificador!="0"):
            todos = todos.exclude(sact_id=identificador)
            for t in todos:
                if t.sact_nombre.lower() == str(nombre).lower():
                    bandera = False
                    msj = "No se ha podido actualizar. Ya existe un sensor con el mimso nombre"
                    break

                if t.id_sensor.lower() == str(id_sensor).lower():
                    bandera = False
                    msj = "No se ha podido actualizar. Ya existe un sensor con el mimso ID"
                    break

            if bandera:
                sensor=sen_sensor_actuador.objects.get(sact_id=identificador)
                sensor.sact_tipo=tipo
                sensor.sact_nombre=nombre
                sensor.id_sensor = id_sensor
                sensor.dis_id_id = dispositivo

                if latitud:
                    sensor.sact_latitud = latitud

                if longitud:
                    sensor.sact_longitud = longitud

                sensor.save()
                messages.add_message(request, messages.SUCCESS, msj)
            else:
                messages.add_message(request, messages.ERROR, msj)
        else:
            for t in todos:
                if t.sact_nombre.lower() == str(nombre).lower():
                    bandera = False
                    msj = "No se ha podido guardar. Ya existe un sensor con el mimso nombre"
                    break

                if t.id_sensor.lower() == str(id_sensor).lower():
                    bandera = False
                    msj = "No se ha podido guardar. Ya existe un sensor con el mimso ID"
                    break

            if bandera:
                sensor = sen_sensor_actuador(sact_tipo=tipo, sact_nombre=nombre, id_sensor=id_sensor, dis_id_id=dispositivo, sact_latitud=latitud, sact_longitud=longitud)
                sensor.save()
                messages.add_message(request, messages.SUCCESS, "Se creo un nuevo dispositivo")
            else:
                messages.add_message(request, messages.ERROR, msj)
    except Exception as exc:
        print(exc)
        messages.add_message(request, messages.ERROR, "Ha ocurrido un error al tratar de ingresar el registro. COD.: "+str(exc.message))

    return redirect("/sensores/index_s/")

def cargarSensor(request):
    #Cargar calles e intersecciones
    datos = []
    try:
        sensor = sen_sensor_actuador.objects.get(sact_id=request.GET['pk'])
        datos.append({'id': sensor.sact_id, 'nombre': sensor.sact_nombre, 'tipo':sensor.sact_tipo, 'id_sensor': sensor.id_sensor,
                      'dispositivo': sensor.dis_id.dis_id})
    except Exception as e:
        print(e.message)
        datos.append({'error': e})
    return HttpResponse(json.dumps(datos), content_type="application/json")


def eliminarSensor(request):
    id_sen = request.POST.get('txt_id')
    try:
        sensor=sen_sensor_actuador.objects.get(sact_id=id_sen)
        sensor.delete()
        messages.add_message(request, messages.SUCCESS, "Se elimino el sensor")
    except Exception as exc:
        messages.add_message(request, messages.ERROR, "Ha ocurrido un error al tratar de ingresar el registro. COD.: "+str(exc.message))

    return redirect("/sensores/index_s/")


def configurarSensor(request):
    try:
        sensor=sen_sensor_actuador.objects.get(sact_id=request.POST.get('txt_id_sensor'))
        dispositivo=sen_dispositivo.objects.get(dis_id=request.POST.get('dispositivos'))
        sensor.dis_id=dispositivo
        sensor.save()
        messages.add_message(request, messages.SUCCESS, "Se configuro su sensor con el dispositivo: "+str(dispositivo.dis_nombre))
    except Exception as exc:
        messages.add_message(request, messages.ERROR, "Ha ocurrido un error al tratar de ingresar el registro. COD.: "+str(exc.message))

    return redirect("/sensores/index_s/")

def guardarDispositivoAjax(request):
    datos = {}
    nombre = request.POST.get('txt_nombre')
    descripcion = request.POST.get('txt_descripcion')
    mac = request.POST.get('txt_mac')
    localidad = request.POST.get('txt_loc')

    try:
        todos = sen_dispositivo.objects.all()
        for t in todos:
            if t.dis_nombre.lower() == str(nombre).lower():
                datos['result'] = "X"
                datos['message'] = "El nombre que escribió ya existe en otro dispositivo"
                return HttpResponse(json.dumps(datos), content_type="application/json")

            if t.dis_mac.lower() == str(mac).lower():
                datos['result'] = "X"
                datos['message'] = "La MAC que escribió ya existe en otro dispositivo"
                return HttpResponse(json.dumps(datos), content_type="application/json")

        dispositivo = sen_dispositivo(dis_nombre=nombre,dis_descripcion=descripcion, dis_mac=mac, dis_localizacion=localidad)
        dispositivo.save()

        datos['message'] = "El dispositivo ha sido guardado correctamente"
        datos['result'] = "OK"
    except Exception as edd:
        datos['result'] = "X"
        datos['message'] = "Ha ocurrido un error al tratar de ingresar el registro. COD.: "+str(edd.message)

    return HttpResponse(json.dumps(datos), content_type="application/json")