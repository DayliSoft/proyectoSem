# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sensores', '0007_remove_sen_dispositivo_ar_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='sen_sensor_actuador',
            name='id_sensor',
            field=models.CharField(default=1, unique=True, max_length=15, verbose_name=b'Id_Sensor'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='sen_sensor_actuador',
            name='sact_nombre',
            field=models.CharField(unique=True, max_length=200, verbose_name=b'Nombre'),
        ),
    ]
