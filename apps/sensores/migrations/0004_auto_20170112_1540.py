# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('geolocalizacion', '0005_auto_20170112_1540'),
        ('sensores', '0003_auto_20170111_2038'),
    ]

    operations = [
        migrations.CreateModel(
            name='sen_calle_interseccion_dashboard_sensor',
            fields=[
                ('scint_id', models.AutoField(serialize=False, verbose_name=b'Id', primary_key=True)),
                ('scint_calle', models.ForeignKey(to='geolocalizacion.geo_calle')),
                ('scint_intereseccion', models.ForeignKey(to='geolocalizacion.geo_interseccion_dashboard')),
            ],
        ),
        migrations.CreateModel(
            name='sen_semaforo',
            fields=[
                ('sem_id', models.OneToOneField(primary_key=True, serialize=False, to='sensores.sen_sensor_actuador', verbose_name=b'sem_id')),
                ('sem_tiempo_cambio', models.IntegerField()),
                ('sem_latitud', models.FloatField(verbose_name=b'Latitud')),
                ('sem_longitud', models.FloatField(verbose_name=b'Longitud')),
            ],
            options={
                'verbose_name_plural': 'Semaforos',
            },
        ),
        migrations.RemoveField(
            model_name='sen_sensor_actuador',
            name='int_id',
        ),
        migrations.AlterField(
            model_name='sen_sensor_actuador',
            name='dis_id',
            field=models.ForeignKey(to='sensores.sen_dispositivo', null=True),
        ),
        migrations.AddField(
            model_name='sen_calle_interseccion_dashboard_sensor',
            name='scint_sensor',
            field=models.ForeignKey(to='sensores.sen_sensor_actuador'),
        ),
    ]
