# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sensores', '0002_auto_20170108_1724'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sen_sensor_actuador',
            name='sact_tipo',
            field=models.CharField(max_length=200, verbose_name=b'Tipo'),
        ),
    ]
