# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sensores', '0004_auto_20170112_1540'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='sen_semaforo',
            name='sem_latitud',
        ),
        migrations.RemoveField(
            model_name='sen_semaforo',
            name='sem_longitud',
        ),
        migrations.AddField(
            model_name='sen_sensor_actuador',
            name='sact_latitud',
            field=models.FloatField(default=1, verbose_name=b'Latitud'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='sen_sensor_actuador',
            name='sact_longitud',
            field=models.FloatField(default=1, verbose_name=b'Longitud'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='sen_semaforo',
            name='sem_tiempo_cambio',
            field=models.IntegerField(default=3),
        ),
    ]
