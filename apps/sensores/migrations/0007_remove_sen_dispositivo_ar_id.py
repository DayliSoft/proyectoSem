# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sensores', '0006_auto_20170113_1203'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='sen_dispositivo',
            name='ar_id',
        ),
    ]
