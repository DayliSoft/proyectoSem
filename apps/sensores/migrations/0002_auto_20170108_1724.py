# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sensores', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sen_sensor_actuador',
            name='int_id',
            field=models.ForeignKey(to='geolocalizacion.geo_interseccion_dashboard'),
        ),
    ]
