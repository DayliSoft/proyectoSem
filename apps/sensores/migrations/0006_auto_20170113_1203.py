# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('geolocalizacion', '0006_auto_20170113_1149'),
        ('sensores', '0005_auto_20170112_1709'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='sen_calle_interseccion_dashboard_sensor',
            name='scint_calle',
        ),
        migrations.AddField(
            model_name='sen_calle_interseccion_dashboard_sensor',
            name='scint_calle_mapa',
            field=models.ForeignKey(default=1, to='geolocalizacion.geo_calle_mapa'),
            preserve_default=False,
        ),
    ]
