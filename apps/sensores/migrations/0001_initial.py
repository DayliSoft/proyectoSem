# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('localizacion', '0001_initial'),
        ('geolocalizacion', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='sen_dispositivo',
            fields=[
                ('dis_id', models.AutoField(serialize=False, verbose_name=b'Id', primary_key=True)),
                ('dis_nombre', models.CharField(max_length=200, verbose_name=b'Nombre')),
                ('dis_mac', models.CharField(max_length=200, verbose_name=b'Mac')),
                ('dis_localizacion', models.CharField(max_length=200, verbose_name=b'Localizacion')),
                ('dis_descripcion', models.CharField(max_length=200, verbose_name=b'Descripcion')),
                ('dis_fecha_registro', models.DateTimeField(auto_now=True)),
                ('ar_id', models.ForeignKey(to='geolocalizacion.geo_mapa')),
            ],
            options={
                'verbose_name_plural': 'Dispositivos',
            },
        ),
        migrations.CreateModel(
            name='sen_sensor_actuador',
            fields=[
                ('sact_id', models.AutoField(serialize=False, verbose_name=b'Id', primary_key=True)),
                ('sact_tipo', models.CharField(max_length=200, verbose_name=b'Nombre')),
                ('sact_nombre', models.CharField(max_length=200, verbose_name=b'Nombre')),
                ('dis_id', models.ForeignKey(to='sensores.sen_dispositivo')),
                ('int_id', models.ForeignKey(to='localizacion.loc_interseccion_dashboard')),
            ],
            options={
                'verbose_name_plural': 'Sensores_Actuadores',
            },
        ),
    ]
