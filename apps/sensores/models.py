from django.db import models
from django.conf import settings
import datetime
from mptt.models import MPTTModel, TreeForeignKey
from apps.localizacion.models import *
from apps.geolocalizacion.models import *

class sen_dispositivo(models.Model):
    dis_id = models.AutoField("Id",primary_key=True)
    dis_nombre=models.CharField("Nombre",max_length=200)
    dis_mac=models.CharField("Mac",max_length=200)
    dis_localizacion=models.CharField("Localizacion",max_length=200)
    dis_descripcion=models.CharField("Descripcion",max_length=200)
    dis_fecha_registro=models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.dis_nombre

    class Meta:
        verbose_name_plural='Dispositivos'
        app_label = 'sensores'

class sen_sensor_actuador(models.Model):
    sact_id = models.AutoField("Id",primary_key=True)
    sact_tipo=models.CharField("Tipo",max_length=200)
    sact_nombre=models.CharField("Nombre",max_length=200, unique=True)
    sact_latitud=models.FloatField("Latitud")
    sact_longitud=models.FloatField("Longitud")
    dis_id=models.ForeignKey(sen_dispositivo, null=True)
    id_sensor = models.CharField("Id_Sensor", unique=True, max_length=15)

    def __unicode__(self):
        return self.sact_nombre

    class Meta:
        verbose_name_plural='Sensores_Actuadores'
        app_label = 'sensores'

class sen_semaforo(models.Model):
    sem_id = models.OneToOneField(sen_sensor_actuador, primary_key=True,verbose_name="sem_id")
    sem_tiempo_cambio = models.IntegerField(default=3)

    def __unicode__(self):
        return self.sem_id.sact_nombre

    class Meta:
        verbose_name_plural='Semaforos'
        app_label = 'sensores'


class sen_calle_interseccion_dashboard_sensor(models.Model):
    scint_id=models.AutoField("Id",primary_key=True)
    scint_calle_mapa=models.ForeignKey(geo_calle_mapa)
    scint_intereseccion=models.ForeignKey(geo_interseccion_dashboard)
    scint_sensor=models.ForeignKey(sen_sensor_actuador)

    def __unicode__(self):
        return self.scint_sensor.sact_nombre
