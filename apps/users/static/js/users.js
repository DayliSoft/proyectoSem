$('#form_res').on("submit", function() {
    event.preventDefault();
    $("#modalEspera").modal({backdrop: 'static', keyboard: false});
    $.ajax({
        type: "POST",
        url: "/users/resec/",
        dataType: 'json',
        data: $(this).serialize(),
        success: function(response) {
            $("#modalEspera").modal("hide");
            console.log(response);
            if (response.msg === 'ok') {
                toastr.options={"progressBar": true};
                toastr.success('Por favor, revise su correo','Info')
            } else {
                toastr.options={"progressBar": true};
                toastr.error(response.msg,'Info')
            }
        },
        error: function(e) {
            $("#modalEspera").modal("hide");
            toastr.options={"progressBar": true};
            toastr.error("Error de conexión",'Info')
        }
    });
});


function usuario_modificar(url){
    if($("#formularioModificar").valid() == true) {
        var form = document.forms.namedItem("formularioModificar");
        datos = new FormData(form);
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            async: true,
            data: datos,
            processData: false,
            contentType: false,
            success: function (json) {
                if (json.result == "OK") {
                    toastr.options = {
                        "progressBar": true,
                        "showDuration": "500",
                        "hideDuration": "3000",
                        "timeOut": "8000",
                        "extendedTimeOut": "3000"
                    };
                    toastr.success(json.message, 'Correcto');
                } else {
                    toastr.options = {
                        "progressBar": true,
                        "showDuration": "500",
                        "hideDuration": "3000",
                        "timeOut": "8000",
                        "extendedTimeOut": "3000"
                    };
                    toastr.error(json.message, 'Estado');
                }
            },
            error: function (e) {
                toastr.warning("Revise que los campos ingresados sean válidos", 'Estado');
                console.log(e);
            }
        });
    }else{
        toastr.options = {
            "progressBar": true,
            "showDuration": "500",
            "hideDuration": "3000",
            "timeOut": "8000",
            "extendedTimeOut": "3000"
        };
        toastr.warning("Revise que los campos ingresados sean válidos", 'Estado');
    }
}

function registrarUsuario() {
    if($("#form_registrar").valid()) {
        if ($("#reg_pass1").val() != $("#reg_pass2").val()) {
            toastr.error('Las contraseñas no coinciden', 'Info');
        } else {
            var form = document.forms.namedItem("form_registrar");
            datos = new FormData(form);
            $('<img id="img_ajax_guardar" src="../../static/assets/images/ajax_load.gif">').insertBefore("#btnRegistrar");

            $.ajax({
                type: "POST",
                url: "/users/crear_cuenta/",
                dataType: "json",
                async: true,
                data: datos,
                processData: false,
                contentType: false,
                success: function (json) {
                    $("#img_ajax_guardar").remove();
                    if (json.result == "OK") {
                        window.location.href = "/";
                    } else {
                        toastr.error('Ha ocurrido un error: ' + json.message, 'Info');
                    }
                },
                error: function (e) {
                    console.log(e);
                    $("#img_ajax_guardar").remove();
                    toastr.error("Lo sentimos", "Ha ocurrido un error, inténtelo más tarde.", 'Info');
                }
            });
        }
    }else{
        toastr.error('Los campos no deben estar vacíos', 'Info');
    }
}