//For getting CSRF token
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

jQuery(document).ready(function() {
    "use strict";
    $('.tagline').addClass('hide');
});

$('#form-reset').on("submit", function() {
    event.preventDefault();
    var csrftoken = getCookie('csrftoken');
    var contrasenia = $('#cont0').val();
    var code = $('#code').val();
    $.ajax({
        type: "POST",
        url: "/users/confirm/",
        dataType: 'json',
        data: {
            contrasenia: contrasenia,
            code: code,
            csrfmiddlewaretoken: csrftoken
        },
        success: function(response) {
            window.location.href = 'http://localhost:8000/users/login';
            toastr.options={"progressBar": true}
            toastr.success('Contraseña reestablecida','Info')
        },

        error: function() {
            console.log('Error');
        }
    });
});

function validarPasswd() {
    var val0 = $('#cont0').val();
    var val1 = $('#cont1').val();
    if (val0 === val1) {
        return true;
    } else {
       toastr.options={"progressBar": true}
        toastr.error('Las contraseñas no coinciden','Info')
        return false;
    }
}