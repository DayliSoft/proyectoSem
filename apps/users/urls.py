from django.conf.urls import include, url
from django.contrib import admin
from apps.users.views import *

urlpatterns = [
    url(r'^login', user_login, name="login"),
    url(r'^salir/$', LogOut, name = 'logout'),
    url(r'^signup', user_signup, name="signup"),
    url(r'^reset', user_recuperar, name="reset"),
    url(r'^resec', user_resetear, name='resetear'),
	url(r'^resepc/(?P<code>\w+)/', reset_pass),
    url(r'^confirm/', reset_confirm),
    url(r'^perfil/', user_perfil),
	url(r'^modificarUsuario/', modificarUsuario, name='modificarUsuario'),
    url(r'^crear_cuenta/', registrar_usuario, name='registrar_usuario'),
]
