# -*- coding: utf-8 -*-

from django.shortcuts import render
from django.shortcuts import redirect
from django.contrib.auth import authenticate, login
from django.contrib import messages
import hashlib, datetime, random
from django.http import JsonResponse
from django.core.mail import send_mail
from .models import User, UserProfile
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, redirect,get_object_or_404,render_to_response
from django.template.loader import render_to_string
from django.utils import timezone
from django.contrib.auth.decorators import login_required
import os
from django.contrib.auth import logout
import json

# Create your views here.
def user_login(request):
    if not request.user.is_authenticated():
        if request.method == "POST":
            user = request.POST['username']
            cont = request.POST['pwd']
            dic = LogIn(request, user,cont)
            if dic['msg'] is 'ok':
                messages.add_message(request, messages.SUCCESS, 'Bienvenido '+user)
            else:
                messages.add_message(request, messages.ERROR, dic['result'])
            return redirect('/')
        else:
            return render(request, 'users/logearse.html')
    else:
        return  redirect("/")

def LogIn(request, username, password):
	user = authenticate(username = username, password = password)
	if user is not None:
		if user.is_active:
			login(request, user)
			dic = {'msg' : 'ok'}
		else:
			dic = {'msg' : 'noActivo', 'result' : 'La cuenta con la que intenta ingresar no esta activada.'}
	else:
		dic = {'msg' : 'noExiste', 'result' : 'La cuenta con la que intenta ingresar no existe.'}
	return dic

def user_signup(request):
    if not request.user.is_authenticated():
        return render(request, 'users/registrarse.html')
    else:
        return redirect("/")


def user_recuperar(request):
    if not request.user.is_authenticated():
        return render(request, 'users/recuperar.html')
    else:
        return redirect("/")

def LogOut(request):
    logout(request)
    return redirect('/')

def user_resetear(request):
    dic = {}
    if request.method == "POST":
        try:
            emailUs = request.POST['email']
            user=User.objects.filter(email=emailUs)
            if user.exists():
                salt = hashlib.sha1(str(random.random())).hexdigest()[:5]
                activation_key = hashlib.sha1(salt+emailUs).hexdigest()
                key_expires = datetime.datetime.today() + datetime.timedelta(2)

                find_user=User.objects.get(email=emailUs)
                # Crear el perfil del usuario
                new_profile = UserProfile(user=find_user, activation_key=activation_key,key_expires=key_expires)
                new_profile.save()

                # Enviar un email de confirmación
                # contexto

                ctx = {
                    'usuario' : user,
                    'code' : activation_key
                }

                asunto = 'Restablecer contrasena'
                msg_plain = render_to_string('template_emails/resetear_cont/index.txt', ctx)
                msg_html = render_to_string('template_emails/resetear_cont/index.html', ctx)
                find_empresa = 'bxbr8792@gmail.com'

                # Enviar un email de confirmación
                send_mail(
                    asunto,
                    msg_plain,
                    find_empresa,
                    [emailUs],
                    html_message=msg_html,
                )
                dic = {'msg':'ok'}
            else:
                dic = {'msg':'No existe un usuario con ese email'}
                print 'No existe un usuario con ese email'
        except Exception, e:
            dic = {'msg':e}
            print e
    response = JsonResponse(dic)
    return HttpResponse(response.content)

def reset_pass(request, code):
    if request.user.is_authenticated():
        HttpResponseRedirect('/')

    # Verifica que el token de activación sea válido y sino retorna un 404
    user_profile = get_object_or_404(UserProfile, activation_key=code)

    # verifica si el token de activación ha expirado y si es así renderiza el html de registro expirado
    if user_profile.key_expires < timezone.now():
        return render_to_response('users/expirado.html')

    dic = {
        'code' : code
    }
    return render_to_response('users/reestablecer.html',dic)



def reset_confirm(request):
    if request.is_ajax():

        try:
            print request.POST
            find_user_profile = UserProfile.objects.get(activation_key = request.POST['code'])
            find_user = find_user_profile.user
            find_user.set_password(request.POST['contrasenia'])
            find_user.save()

            dic = {'msg' : 'ok'}

            find_user_profile.delete()

        except Exception, e:
            print e
            dic = {'msg' : 'err'}

        response = JsonResponse(dic)
        return HttpResponse(response.content)

    else:
        HttpResponseRedirect('/')

@login_required(login_url='/')
def user_perfil(request):
    return render(request, 'users/perfil.html')

def modificarUsuario(request):
    datos = {}
    usuario = request.POST.get('username')
    # contrasena = request.POST.get('contrasena')
    nombre = request.POST.get('nombre')
    apellido = request.POST.get('apellido')
    email = request.POST.get('email')
    telefono = request.POST.get('telefono')
    avatar = request.FILES.get('id_avatar_usuario')

    mensaje ="El registro ha sido creado correctamente"

    try:
        usuario = User.objects.get(id=request.user.id)
        # usuario.username = usuario
        # usuario.password = contrasena
        usuario.us_nombre = nombre
        usuario.us_apellidos = apellido
        usuario.us_telefono_movil = telefono
        usuario.email = email
        if avatar:
            if usuario.us_avatar:
                os.remove(usuario.us_avatar.path)
            usuario.us_avatar = avatar

        mensaje = "El registro ha sido actualizado correctamente"
        usuario.save()

        datos['message'] = mensaje
        datos['result'] = "OK"
    except Exception as e:
        print(e)
        datos['message'] = "Ha ocurrido un error al tratar de ingresar el registro. COD.: "+str(e.message)
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")

def registrar_usuario(request):
    datos = {}
    if request.method == "POST" and request.is_ajax():
        try:
            listaRegistrados = User.objects.all()

            for l in listaRegistrados:
                if l.username == request.POST['reg_usuario']:
                    datos['message'] = "Ya existe un usuario con ese nombre."
                    datos['result'] = "X"
                    return HttpResponse(json.dumps(datos), content_type="application/json")

                if l.email == request.POST['reg_email']:
                    datos['message'] = "Ya existe un usuario con ese correo."
                    datos['result'] = "X"
                    return HttpResponse(json.dumps(datos), content_type="application/json")

            User.objects.create_user(username=request.POST['reg_usuario'],
                                     email=request.POST['reg_email'],
                                     us_nombre=request.POST['reg_nombre'],
                                     us_apellidos=request.POST['reg_apellido'],
                                     us_telefono_movil=request.POST['reg_telefono'],
                                     password=request.POST['reg_pass1'])

            user = authenticate(username=request.POST['reg_usuario'], password=request.POST['reg_pass1'])
            login(request, user)

            messages.add_message(request, messages.SUCCESS, 'Bienvenido ' + request.POST['reg_usuario'] )
            datos['result'] = "OK"
        except Exception, e:
            datos['message'] = "Ha ocurrido un error al tratar de crear la cuenta. COD.: " + str(e.message)
            datos['result'] = "X"

        return HttpResponse(json.dumps(datos), content_type="application/json")

    return render(request, 'users/registrarse.html')
