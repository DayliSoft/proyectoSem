# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0006_require_contenttypes_0002'),
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(null=True, verbose_name='last login', blank=True)),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('username', models.CharField(help_text=b'Debe ser \xc3\xbanico, sin espacios ni caracteres especiales.', unique=True, max_length=100, verbose_name=b'Username')),
                ('us_nombre', models.CharField(help_text=b'Nombres del usuario.', max_length=100, verbose_name=b'Nombres')),
                ('us_apellidos', models.CharField(help_text=b'Apellidos del usuario.', max_length=100, verbose_name=b'Apellidos')),
                ('us_telefono_movil', models.CharField(help_text=b'Telefono o movil del usuario.', max_length=10, verbose_name=b'Telefono')),
                ('us_avatar', models.ImageField(upload_to=b'users', null=True, verbose_name=b'Avatar', blank=True)),
                ('email', models.EmailField(help_text=b'Cuenta de mail del usuario, debe ser unica.', unique=True, max_length=254, verbose_name=b'Email')),
                ('is_active', models.BooleanField(default=True, help_text=b'Designa si el usuario debe ser tratado como activo.')),
                ('is_staff', models.BooleanField(default=False, help_text=b'Indica si el usuario puede iniciar sesi\xc3\xb3n en el sitio de administrac\xc3\xadon.')),
                ('groups', models.ManyToManyField(related_query_name='user', related_name='user_set', to='auth.Group', blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', verbose_name='groups')),
                ('user_permissions', models.ManyToManyField(related_query_name='user', related_name='user_set', to='auth.Permission', blank=True, help_text='Specific permissions for this user.', verbose_name='user permissions')),
            ],
            options={
                'verbose_name_plural': 'Usuarios',
            },
        ),
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('activation_key', models.CharField(max_length=40, blank=True)),
                ('key_expires', models.DateTimeField(default=datetime.date(2016, 12, 26))),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name_plural': 'Perfiles de Usuario',
            },
        ),
    ]
