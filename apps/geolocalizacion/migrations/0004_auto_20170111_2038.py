# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('geolocalizacion', '0003_auto_20170108_1724'),
    ]

    operations = [
        migrations.AlterField(
            model_name='geo_calle',
            name='cal_principal',
            field=models.BooleanField(default=False),
        ),
    ]
