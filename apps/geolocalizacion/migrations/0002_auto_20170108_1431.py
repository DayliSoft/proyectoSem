# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('geolocalizacion', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='geo_mapa',
            name='map_latitud',
            field=models.FloatField(verbose_name=b'Latitud'),
        ),
        migrations.AlterField(
            model_name='geo_mapa',
            name='map_longitud',
            field=models.FloatField(verbose_name=b'Longitud'),
        ),
        migrations.AlterField(
            model_name='geo_marker',
            name='mar_latitud',
            field=models.FloatField(verbose_name=b'Latitud'),
        ),
        migrations.AlterField(
            model_name='geo_marker',
            name='mar_longitud',
            field=models.FloatField(verbose_name=b'Longitud'),
        ),
    ]
