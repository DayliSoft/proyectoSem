# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('geolocalizacion', '0005_auto_20170112_1540'),
    ]

    operations = [
        migrations.CreateModel(
            name='geo_calle_mapa',
            fields=[
                ('cmap_id', models.AutoField(serialize=False, primary_key=True)),
            ],
            options={
                'verbose_name_plural': 'Mapas-Calles',
            },
        ),
        migrations.RemoveField(
            model_name='geo_calle',
            name='cal_mapa',
        ),
        migrations.AddField(
            model_name='geo_calle_mapa',
            name='cmap_calle',
            field=models.ForeignKey(related_name='callesMapas', to='geolocalizacion.geo_calle'),
        ),
        migrations.AddField(
            model_name='geo_calle_mapa',
            name='cmap_mapa',
            field=models.ForeignKey(related_name='Mapas', to='geolocalizacion.geo_mapa'),
        ),
    ]
