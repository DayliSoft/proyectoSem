# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('geolocalizacion', '0006_auto_20170113_1149'),
    ]

    operations = [
        migrations.AlterField(
            model_name='geo_interseccion_dashboard',
            name='int_nombre',
            field=models.CharField(unique=True, max_length=100),
        ),
    ]
