# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('geolocalizacion', '0002_auto_20170108_1431'),
    ]

    operations = [
        migrations.CreateModel(
            name='geo_calle',
            fields=[
                ('cal_id', models.AutoField(serialize=False, primary_key=True)),
                ('cal_nombre', models.CharField(max_length=100)),
                ('cal_principal', models.BooleanField()),
            ],
            options={
                'verbose_name_plural': 'Calles',
            },
        ),
        migrations.CreateModel(
            name='geo_interseccion_dashboard',
            fields=[
                ('int_id', models.AutoField(serialize=False, primary_key=True)),
                ('int_nombre', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name_plural': 'Intersecciones',
            },
        ),
        migrations.CreateModel(
            name='geo_semaforo',
            fields=[
                ('sem_id', models.AutoField(serialize=False, primary_key=True)),
                ('sem_ubicacion', models.CharField(max_length=100)),
                ('sem_tiempo_cambio', models.IntegerField()),
                ('sem_calle', models.ForeignKey(to='geolocalizacion.geo_calle')),
            ],
            options={
                'verbose_name_plural': 'Semaforos',
            },
        ),
        migrations.AddField(
            model_name='geo_calle',
            name='cal_int',
            field=models.ForeignKey(to='geolocalizacion.geo_interseccion_dashboard'),
        ),
        migrations.AddField(
            model_name='geo_calle',
            name='cal_mapa',
            field=models.ForeignKey(to='geolocalizacion.geo_mapa'),
        ),
    ]
