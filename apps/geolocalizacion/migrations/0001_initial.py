# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('localizacion', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='geo_mapa',
            fields=[
                ('map_id', models.AutoField(serialize=False, verbose_name=b'Id', primary_key=True)),
                ('map_nombre', models.CharField(max_length=200, verbose_name=b'Nombre')),
                ('map_descripcion', models.CharField(max_length=200, verbose_name=b'Descripcion')),
                ('map_latitud', models.IntegerField(verbose_name=b'Latitud')),
                ('map_longitud', models.IntegerField(verbose_name=b'Longitud')),
                ('map_favorito', models.BooleanField(default=True)),
                ('map_localidad', models.ForeignKey(to='localizacion.loc_localidad', null=True)),
            ],
            options={
                'verbose_name': 'Mapa',
                'verbose_name_plural': 'Mapas',
            },
        ),
        migrations.CreateModel(
            name='geo_marker',
            fields=[
                ('mar_id', models.AutoField(serialize=False, verbose_name=b'Id', primary_key=True)),
                ('mar_nombre', models.CharField(max_length=200, verbose_name=b'Nombre')),
                ('mar_descripcion', models.CharField(max_length=200, verbose_name=b'Descripcion')),
                ('mar_latitud', models.IntegerField(verbose_name=b'Latitud')),
                ('mar_longitud', models.IntegerField(verbose_name=b'Longitud')),
                ('mar_mapa', models.ForeignKey(related_name='markers', to='geolocalizacion.geo_mapa')),
            ],
            options={
                'verbose_name': 'Marker',
                'verbose_name_plural': 'Marcadores',
            },
        ),
    ]
