# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('geolocalizacion', '0004_auto_20170111_2038'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='geo_semaforo',
            name='sem_calle',
        ),
        migrations.RemoveField(
            model_name='geo_calle',
            name='cal_int',
        ),
        migrations.DeleteModel(
            name='geo_semaforo',
        ),
    ]
