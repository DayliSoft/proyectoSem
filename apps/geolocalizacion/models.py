from django.db import models
from django.conf import settings
import datetime
from apps.localizacion.models import *

class geo_mapa(models.Model):
    map_id = models.AutoField("Id",primary_key=True)
    map_nombre=models.CharField("Nombre",max_length=200)
    map_descripcion=models.CharField("Descripcion",max_length=200)
    map_latitud=models.FloatField("Latitud")
    map_longitud=models.FloatField("Longitud")
    map_favorito=models.BooleanField(default=True)
    map_localidad=models.ForeignKey(loc_localidad, null=True)

    class Meta:
        app_label = 'geolocalizacion'
        verbose_name = "Mapa"
        verbose_name_plural = "Mapas"
    def __unicode__(self):
        return self.map_nombre

class geo_marker(models.Model):
    mar_id = models.AutoField("Id",primary_key=True)
    mar_nombre=models.CharField("Nombre",max_length=200)
    mar_descripcion=models.CharField("Descripcion",max_length=200)
    mar_latitud=models.FloatField("Latitud")
    mar_longitud=models.FloatField("Longitud")
    mar_mapa=models.ForeignKey(geo_mapa, related_name='markers')

    class Meta:
        app_label = 'geolocalizacion'
        verbose_name = "Marker"
        verbose_name_plural = "Marcadores"

    def __unicode__(self):
        return self.mar_nombre

class geo_interseccion_dashboard(models.Model):
    int_id = models.AutoField(primary_key=True)
    int_nombre = models.CharField(max_length=100, unique=True)

    def __unicode__(self):
        return self.int_nombre

    class Meta:
        verbose_name_plural='Intersecciones'
        app_label = 'geolocalizacion'

class geo_calle(models.Model):
    cal_id = models.AutoField(primary_key=True)
    cal_nombre = models.CharField(max_length=100)
    cal_principal = models.BooleanField(default=False)
    # cal_int=models.ForeignKey(geo_interseccion_dashboard)

    def __unicode__(self):
        return self.cal_nombre

    class Meta:
        verbose_name_plural='Calles'
        app_label = 'geolocalizacion'

class geo_calle_mapa(models.Model):
    cmap_id = models.AutoField(primary_key=True)
    cmap_calle=models.ForeignKey(geo_calle, related_name='callesMapas')
    cmap_mapa=models.ForeignKey(geo_mapa, related_name='Mapas')

    def __unicode__(self):
        return self.cmap_mapa.map_nombre+'-'+str(self.cmap_calle.cal_nombre)

    class Meta:
        verbose_name_plural='Mapas-Calles'
        app_label = 'geolocalizacion'