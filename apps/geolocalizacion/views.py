# -*- coding: utf-8 -*-

from django.shortcuts import render
from django.shortcuts import redirect
from django.contrib.auth import authenticate, login
from django.contrib import messages
import hashlib, datetime, random
from django.http import JsonResponse
from django.core.mail import send_mail
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, redirect,get_object_or_404,render_to_response
from django.template.loader import render_to_string
from django.utils import timezone
from django.contrib.auth.decorators import login_required
import os
from django.contrib.auth import logout
import json

from apps import sensores
from apps.localizacion.models import *
from apps.geolocalizacion.models import *
from apps.sensores.models import *
from django.shortcuts import render, redirect, get_object_or_404, render_to_response, RequestContext
from django.db import transaction, IntegrityError


def inicio(request):
    if not request.user.is_authenticated():
        return redirect("/")

    calles=""
    for i in geo_calle.objects.all():
        calles=calles+','+str(i.cal_nombre)
    intersecciones=""
    for i in geo_interseccion_dashboard.objects.all():
        intersecciones=intersecciones+','+str(i.int_nombre)

    dispositivos = sen_dispositivo.objects.all().order_by("-dis_id")

    cxt = {
    'calles': calles,
    'ubicaciones': loc_localidad.objects.all(),
    'mapas': geo_mapa.objects.all(),
    'intersecciones': intersecciones,
    'dispositivos': dispositivos
    }
    return render_to_response('geolocalizacion/index.html', context_instance=RequestContext(request, cxt))

def agregarUbicacion(request):
    datos = {}

    transaction.set_autocommit(False)
    try:
        nombre = request.POST.get('txtNombre')
        lugar = request.POST.get('id_categoria_padre_mapa')
        descripcion = request.POST.get('txtDescripcion')
        latitud = request.POST.get('latitud')
        longitud = request.POST.get('longitud')
        mensaje = "Se ha guardado el mapa "
        print(request.POST.get('id_mapa'))

        if not request.POST.get('id_mapa'):
            id_mapa = 0
        else:
            id_mapa = int(request.POST.get('id_mapa'))
        print(id_mapa,"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa")
        result = json.loads(request.POST.get('marcadores'))

        # Verificar si hay id_mapa <- Actualizar != Guardar
        todos_mapas = geo_mapa.objects.all().exclude(map_id=id_mapa)
        for m in todos_mapas:
            if m.map_nombre.lower() == str(nombre).lower():
                datos['message'] = "Ya existe un mapa con ese nombre"
                datos['result'] = "x"
                return HttpResponse(json.dumps(datos), content_type="application/json")


        if id_mapa:
            mapa = geo_mapa.objects.get(map_id=id_mapa)
            mensaje = "Se ha actualizado el mapa "
        else:
            mapa = geo_mapa()

        mapa.map_nombre=nombre
        mapa.map_descripcion=descripcion
        mapa.map_latitud=latitud
        mapa.map_longitud=longitud
        if(lugar!='0'):
            mapa.map_localidad=loc_localidad.objects.get(loc_id=lugar)
        mapa.save()

        #Lista para guardar las calles/intersecciones
        calles_nuevas = []
        intersecciones_nuevas = []

        if(result):
            for mark in result:
                #calles
                #PREGUNTAR SI LA CALLE YA EXISTE
                banderaCalle=False
                id_calle = 0
                id_inter = 0
                id_sensor = 0

                for i in geo_calle.objects.all():
                    if((i.cal_nombre).lower()==(mark[1]).lower()):
                        id_calle = i.cal_id
                        banderaCalle=True
                        break

                if(banderaCalle):
                    calle=geo_calle.objects.get(cal_id=id_calle)
                else:
                    #Crea la calle/s
                    calle=geo_calle()
                    calle.cal_nombre=mark[1].lower()
                    if (mark[2]==True):
                        print(mark[2],'el checke')
                        calle.cal_principal=True
                    calle.save()

                #sensor
                banderaSensores = False
                for s in sen_sensor_actuador.objects.all():
                    if ((s.sact_nombre).lower() == (mark[0]).lower()):
                        id_sensor = s.sact_id
                        banderaSensores = True
                        break
                if(not banderaSensores):
                    calleMapa=geo_calle_mapa()
                    calleMapa.cmap_calle=calle
                    calleMapa.cmap_mapa=mapa
                    calleMapa.save()

                # PREGUNTAR SI LA INTERSECCION YA EXISTE
                banderaInterseccion=False
                for i in geo_interseccion_dashboard.objects.all():
                    if((i.int_nombre).lower()==(mark[3]).lower()):
                        id_inter = i.int_id
                        banderaInterseccion=True
                        break

                if(banderaInterseccion):
                    interseccion=geo_interseccion_dashboard.objects.get(int_id=id_inter)
                else:
                    # Crea la interseccion/s
                    interseccion=geo_interseccion_dashboard()
                    interseccion.int_nombre=mark[3].lower()
                    interseccion.save()

                if (banderaSensores):
                    sensor = sen_sensor_actuador.objects.get(sact_id=id_sensor)
                else:
                    sensor=sen_sensor_actuador()
                    sensor.sact_tipo=mark[4]
                    sensor.sact_nombre=mark[0]
                    sensor.sact_latitud=mark[5]
                    sensor.sact_longitud=mark[6]
                    sensor.dis_id_id = int(mark[7])
                    sensor.id_sensor = mark[8]
                    sensor.save()

                #TABLA CALLE - INTERESECCION - DASHBOARD
                if not sen_calle_interseccion_dashboard_sensor.objects.filter(scint_sensor = sensor).exists():
                    tabla_intermedia=sen_calle_interseccion_dashboard_sensor()
                    tabla_intermedia.scint_calle_mapa=calleMapa
                    tabla_intermedia.scint_intereseccion=interseccion
                    tabla_intermedia.scint_sensor=sensor
                    tabla_intermedia.save()

                    #SEMAFORO
                    if(mark[4]=="semaforo"):
                        semaforo=sen_semaforo()
                        semaforo.sem_id=sensor
                        semaforo.save()
                # marker=geo_marker(mar_longitud=mark[1], mar_latitud=mark[0],
                #                     mar_nombre=mark[2], mar_descripcion=mark[3], mar_mapa=mapa)
                # marker.save()

            # Guarda en una lista las calles
            for c in geo_calle.objects.all():
                calles_nuevas.append(c.cal_nombre)

            # Guarda en una lista las intersecciones
            for i in geo_interseccion_dashboard.objects.all():
                intersecciones_nuevas.append(i.int_nombre)

            mensaje += "y los sensores correctamente"

        transaction.commit()
        datos['message'] = mensaje
        datos['lista_calles'] = calles_nuevas
        datos['lista_intersecciones'] = intersecciones_nuevas
        datos['id_mapa'] = mapa.map_id
        datos['result'] = "OK"
    except IntegrityError as error:
        transaction.rollback()
        datos['message'] = "Error (Transaction):" + str(error)
        datos['result'] = "x"
    except Exception as error:
        transaction.rollback()
        datos['message'] = "Error:" + str(error)
        datos['result'] = "x"

    return HttpResponse(json.dumps(datos), content_type="application/json")

def eliminarMapa(request):
    datos = {}
    try:
        ubi = geo_mapa.objects.get(map_id=request.GET['idEliminar'])
        ubi.delete()
        datos['message'] = "Su mapa se ha eliminado"
        datos['result'] = "OK"
    except Exception as exc:
        datos['message'] = "Ha ocurrido un error al tratar de ingresar el registro. COD.: "+str(exc)
        datos['result'] = "X"
    return HttpResponse(json.dumps(datos), content_type="application/json")


def eliminarTodo(request):
    datos = {}
    try:
        calleEliminar=geo_mapa.objects.all().delete()
        # # ubi = geo_mapa.objects.all()
        # if ubi.exists():
        #     ubi.delete()
        datos['message'] = "Sus mapas se han eliminado"
        datos['result'] = "OK"
    except Exception as exc:
        datos['message'] = "Ha ocurrido un error al tratar de ingresar el registro. COD.: "+str(exc)
        datos['result'] = "X"

    return HttpResponse(json.dumps(datos), content_type="application/json")


def verMarkers(request):
    datos = []
    try:
        mapa = geo_mapa.objects.get(map_id=request.GET['pk'])
        calles = geo_calle_mapa.objects.filter(cmap_mapa=mapa)
        # interseccion=geo_calle_mapa.objects.filter(cmap_mapa=mapa)
        print(calles)
        sensores=sen_calle_interseccion_dashboard_sensor.objects.filter(scint_calle_mapa=calles)
        for i in sensores:
            print(i.scint_calle_mapa.cmap_mapa.map_nombre)
        # mark=geo_marker.objects.filter(mar_mapa=ubi)
        for i in sensores:
            datos.append({'latitud': i.scint_sensor.sact_latitud,
                 'longitud': i.scint_sensor.sact_longitud,
                 'nombre': i.scint_sensor.sact_nombre,
                 'calle': i.scint_calle_mapa.cmap_calle.cal_nombre,
                 'interseccion': i.scint_intereseccion.int_nombre,
                 'principal': i.scint_calle_mapa.cmap_calle.cal_principal,
                 'id': i.scint_sensor.sact_id,
                 'tipo': i.scint_sensor.sact_tipo})
    except Exception as exc:
        print(exc)
    return HttpResponse(json.dumps(datos), content_type="application/json")


def cargar_intersecciones(request):
    datos = []
    try:
        calles = geo_interseccion_dashboard.objects.all()
        for x in calles:
            datos.append({'id': x.int_id, 'text':x.int_nombre})
    except Exception as e:
        print(e.message)
        datos.append({'error': e})
    return HttpResponse(json.dumps(datos), content_type="application/json")


def agregarCalles(request):
    datos = {}
    try:
        calleEliminar=geo_calle.objects.all().delete()
        nombre = request.POST.get('calle_nombre')
        lugar = request.POST.get('id_categoria_padre1')
        identificador = request.POST.get('identificador')
        print(identificador,'id')
        otra = request.POST.get('interseccion')
        principal=request.POST.get('switch')
        if(lugar!='0'):
            if(lugar==None):
                interseccion=geo_interseccion_dashboard(int_nombre=otra)
                interseccion.save()
            else:
                interseccion=geo_interseccion_dashboard.objects.get(int_id=lugar)
        else:
            interseccion=geo_interseccion_dashboard(int_nombre=otra)
            interseccion.save()
        calle=geo_calle()
        calle.cal_nombre=nombre
        calle.cal_mapa=geo_mapa.objects.get(map_id=identificador)
        calle.cal_int=interseccion
        if(principal):
            calle.cal_principal=True
        calle.save()
        # messages.add_message(request, messages.SUCCESS, "Calles e intersecciones agregadas")
        datos['message'] = "Calles e intersecciones agregadas"
        datos['result'] = "OK"
    except Exception as error:
        print(error)
        datos['message'] = "Error:"+str(error)
        datos['result'] = "x"
        # messages.add_message(request, messages.ERROR, 'Error del tipo: '+str(error))
        # return redirect("/maps/index/")
    # return redirect("/maps/index/")
    return HttpResponse(json.dumps(datos), content_type="application/json")

def verDatosCalles(request):
    datos = []
    try:
        print(request.GET['pk'])
        mapa = geo_mapa.objects.get(map_id=request.GET['pk'])
        calle=geo_calle.objects.get(cal_mapa=mapa)
        datos.append({'id': calle.cal_id, 'nombre':calle.cal_nombre,
                    'principal': calle.cal_principal, 'interseccion':calle.cal_int.int_id})
    except Exception as e:
        print(e.message)
        datos.append({'error': e})
    return HttpResponse(json.dumps(datos), content_type="application/json")

def calles(request):
    if not request.user.is_authenticated():
        return redirect("/")

    cxt = {
    'calles': geo_calle.objects.all(),
    'intersecciones': geo_interseccion_dashboard.objects.all()
    }
    return render_to_response('geolocalizacion/calles.html', context_instance=RequestContext(request, cxt))

def cargarCalles(request):
    datos = []
    try:
        calle = geo_calle.objects.get(cal_id=request.GET['pk'])
        datos.append({'id': calle.cal_id, 'nombre': calle.cal_nombre, 'principal': calle.cal_principal})
    except Exception as e:
        print(e.message)
        datos.append({'error': e})
    return HttpResponse(json.dumps(datos), content_type="application/json")


def eliminarCalle(request):
    datos = {}
    id_cal = request.POST.get('txt_id_calle')
    print(id_cal)
    try:
        calle=geo_calle.objects.get(cal_id=id_cal)
        calle.delete()
        messages.add_message(request, messages.SUCCESS, "Se elimino la Calle")
    except Exception as exc:
        messages.add_message(request, messages.ERROR, "Ha ocurrido un error al tratar de ingresar el registro. COD.: "+str(exc.message))

    return redirect("/maps/calles/")

def guardarCalle(request):
    datos = {}
    nombre = request.POST.get('txt_nombre_calle')
    principal = request.POST.get('switch')
    identificador = request.POST.get('identificadorCalle')
    try:
        if(identificador!="0"):
            calle=geo_calle.objects.get(cal_id=identificador)
            calle.cal_nombre=nombre
            if (principal):
                calle.cal_principal=True
            else:
                calle.cal_principal=False
            calle.save()
            messages.add_message(request, messages.SUCCESS, "El registro se actualizo")
        else:
            calle = geo_calle(cal_nombre=nombre)
            calle.cal_nombre=nombre
            if (principal):
                calle.cal_principal=True
            calle.save()
            messages.add_message(request, messages.SUCCESS, "Se creo un nueva calle")
    except Exception as exc:
        messages.add_message(request, messages.ERROR, "Ha ocurrido un error al tratar de ingresar el registro. COD.: "+str(exc))

    return redirect("/maps/calles/")

def eliminarInterseccion(request):
    datos = {}
    id_cal = request.POST.get('txt_id_interseccion')
    try:
        inter=geo_interseccion_dashboard.objects.get(int_id=id_cal)
        inter.delete()
        messages.add_message(request, messages.SUCCESS, "Se elimino la Interseccion")
    except Exception as exc:
        messages.add_message(request, messages.ERROR, "Ha ocurrido un error al tratar de ingresar el registro. COD.: "+str(exc.message))

    return redirect("/maps/calles/")


def guardarInterseccion(request):
    datos = {}
    nombre = request.POST.get('txt_nombre_interseccion')
    identificador = request.POST.get('identificadorInterseccion')
    try:
        if(identificador!="0"):
            inter=geo_interseccion_dashboard.objects.get(int_id=identificador)
            inter.int_nombre=nombre
            inter.save()
            messages.add_message(request, messages.SUCCESS, "El registro se actualizo")
        else:
            inter = geo_interseccion_dashboard()
            inter.int_nombre=nombre
            inter.save()
            messages.add_message(request, messages.SUCCESS, "Se creo un nueva interseccion")
    except Exception as exc:
        messages.add_message(request, messages.ERROR, "Ha ocurrido un error al tratar de ingresar el registro. COD.: "+str(exc))

    return redirect("/maps/calles/")

def cargarIntersecciones(request):
    datos = []
    try:
        inter = geo_interseccion_dashboard.objects.get(int_id=request.GET['pk'])
        datos.append({'id': inter.int_id, 'nombre': inter.int_nombre,})
    except Exception as e:
        print(e.message)
        datos.append({'error': e})
    return HttpResponse(json.dumps(datos), content_type="application/json")

def comprobarIDSensor(request):
    datos = {}
    idSensor = request.GET.get("id_sensor")
    nombre = request.GET.get("nombre")
    todos = sen_sensor_actuador.objects.all()
    mensaje = "OK"
    resultado = "OK"

    for t in todos:
        if t.sact_nombre.lower() == str(nombre).lower():
            mensaje = "El nombre que escribió ya existe en otro sensor"
            resultado = "X"
            break

        if t.id_sensor.lower() == str(idSensor).lower():
            mensaje = "El ID que escribió ya existe en otro sensor"
            resultado = "X"
            break

    datos["result"] = resultado
    datos["message"] = mensaje

    return HttpResponse(json.dumps(datos), content_type="application/json")

def guardarDispositivoAjax(request):
    datos = {}
    nombre = request.POST.get('txt_nombre_modal')
    descripcion = request.POST.get('txt_descripcion_modal')
    mac = request.POST.get('txt_mac_modal')
    localidad = request.POST.get('txt_loc_modal')

    try:
        todos = sen_dispositivo.objects.all()
        for t in todos:
            if t.dis_nombre.lower() == str(nombre).lower():
                datos['result'] = "X"
                datos['message'] = "El nombre que escribió ya existe en otro dispositivo"
                return HttpResponse(json.dumps(datos), content_type="application/json")

            if t.dis_mac.lower() == str(mac).lower():
                datos['result'] = "X"
                datos['message'] = "La MAC que escribió ya existe en otro dispositivo"
                return HttpResponse(json.dumps(datos), content_type="application/json")

        dispositivo = sen_dispositivo(dis_nombre=nombre, dis_descripcion=descripcion, dis_mac=mac, dis_localizacion=localidad)
        dispositivo.save()

        datos['message'] = "El dispositivo ha sido guardado correctamente"
        datos['result'] = "OK"
    except Exception as edd:
        print(edd)
        datos['result'] = "X"
        datos['message'] = str(edd.message)

    return HttpResponse(json.dumps(datos), content_type="application/json")

def cargarDispositivosSelect(request):
    datos = []
    dispositivos = sen_dispositivo.objects.all().order_by("-dis_id")
    for a in dispositivos:
        datos.append({'id': a.dis_id, 'text': a.dis_nombre})
    return HttpResponse(json.dumps(datos), content_type="application/json")

def guardarDependenciaModal(request):
    datos = {}
    nombre = request.POST.get('txt_ubicacion_modal')
    dependencia = request.POST.get('select_dependencia_modal')

    try:
        todos = loc_localidad.objects.all()
        for t in todos:
            if t.loc_nombre.lower() == str(nombre).lower():
                datos['result'] = "X"
                datos['message'] = "El nombre que escribió ya existe en otra ubicación"
                return HttpResponse(json.dumps(datos), content_type="application/json")

        ubicacion = loc_localidad()
        ubicacion.loc_nombre=nombre
        if(int(dependencia) != 0):
            ubicacion.loc_id_padre_id = int(dependencia)
        ubicacion.save()

        datos['message'] = "La ubicación ha sido guardada correctamente"
        datos['result'] = "OK"
    except Exception as edd:
        datos['result'] = "X"
        datos['message'] = str(edd.message)

    return HttpResponse(json.dumps(datos), content_type="application/json")

def guardarCalleAjax(request):
    datos = {}
    nombre = request.POST.get('txt_nombre_calle')
    principal = request.POST.get('switch')

    try:
        todos = geo_calle.objects.all()
        for t in todos:
            if t.cal_nombre.lower() == str(nombre).lower():
                datos['result'] = "X"
                datos['message'] = "El nombre que escribió ya existe en otra calle"
                return HttpResponse(json.dumps(datos), content_type="application/json")

        calle = geo_calle()
        calle.cal_nombre = nombre

        if principal:
            calle.cal_principal = True

        calle.save()

        datos['message'] = "La calle ha sido guardada correctamente"
        datos['result'] = "OK"
    except Exception as edd:
        datos['result'] = "X"
        datos['message'] = "Ha ocurrido un error al tratar de ingresar el registro. COD.: "+str(edd.message)

    return HttpResponse(json.dumps(datos), content_type="application/json")

def guardarInterseccionAjax(request):
    datos = {}
    nombre = request.POST.get('txt_nombre_interseccion')

    try:
        todos = geo_interseccion_dashboard.objects.all()
        for t in todos:
            if t.int_nombre.lower() == str(nombre).lower():
                datos['result'] = "X"
                datos['message'] = "El nombre que escribió ya existe en otra intersección"
                return HttpResponse(json.dumps(datos), content_type="application/json")

        inter = geo_interseccion_dashboard()
        inter.int_nombre = nombre
        inter.save()
        datos['message'] = "La intersección ha sido guardada correctamente"
        datos['result'] = "OK"
    except Exception as edd:
        datos['result'] = "X"
        datos['message'] = "Ha ocurrido un error al tratar de ingresar el registro. COD.: " + str(edd.message)

    return HttpResponse(json.dumps(datos), content_type="application/json")

def cargar_mapa_modal(request):
    datos = []
    mapas = geo_mapa.objects.get(map_id=int(request.GET.get("id")))
    datos.append({'nombre': mapas.map_nombre, 'descripcion': mapas.map_descripcion, 'localidades': mapas.map_localidad_id,
                  'latitud': mapas.map_latitud, 'longitud': mapas.map_longitud, 'localidad': mapas.map_localidad.loc_nombre,
                  'contador': mapas.Mapas.all().count()})

    return HttpResponse(json.dumps(datos), content_type="application/json")

def getSensor(request):
    datos = []
    sensor = sen_sensor_actuador.objects.get(sact_id=int(request.GET.get("id")))
    tabla = sen_calle_interseccion_dashboard_sensor.objects.get(scint_sensor=sensor)
    intersecciones = tabla.scint_intereseccion.int_nombre
    calles = tabla.scint_calle_mapa.cmap_calle.cal_nombre
    principal = tabla.scint_calle_mapa.cmap_calle.cal_principal

    datos.append({'nombre': sensor.sact_nombre, 'sensor_id': sensor.id_sensor, 'calle': calles, 'interseccion': intersecciones,
                  'principal': principal, 'dispositivo': sensor.dis_id.dis_id, 'tipo': sensor.sact_tipo, 'id':sensor.sact_id})

    return HttpResponse(json.dumps(datos), content_type="application/json")

def eliminarSensor(request):
    print("aaaaaaaaa")

    datos = {}
    id = request.GET.get('id')

    try:
        sensor_eliminar = sen_sensor_actuador.objects.get(sact_id=int(id))
        sensor_eliminar.delete()
        print("elliii")
        datos['message'] = "El sensor se ha eliminado"
        datos['result'] = "OK"
    except Exception as edd:
        datos['result'] = "X"
        datos['message'] = "Ha ocurrido un error al tratar de ingresar el registro. COD.: " + str(edd.message)

    return HttpResponse(json.dumps(datos), content_type="application/json")

def editarSensor(request):
    datos = {}

    transaction.set_autocommit(False)
    try:
        nombre = request.POST.get('txt_semaforo_nombre')
        id_sensor = request.POST.get('txt_id_sensor')
        principal = request.POST.get('switch1')
        calles = request.POST.get('input-tags')
        intersecciones = request.POST.get('input-tags1')
        dispositivo = request.POST.get('select_dispositivo')
        tipo = request.POST.get('select_tipo')
        id = request.POST.get('id')
        mensaje = "Se ha actualizado el sensor correctamente"

        sensor = sen_sensor_actuador.objects.get(sact_id=id)

        #Lista para guardar las calles/intersecciones
        calles_nuevas = []
        intersecciones_nuevas = []

        #calles
        #PREGUNTAR SI LA CALLE YA EXISTE
        banderaCalle=False
        id_inter = 0

        for i in geo_calle.objects.all():
            if((i.cal_nombre).lower()== str(calles).lower()):
                calle=geo_calle.objects.get(cal_id=i.cal_id)
                break

        if(banderaCalle==False):
            #Crea la calle/s
            calle=geo_calle()
            calle.cal_nombre=str(calles).lower()
            if (principal=='on'):
                calle.cal_principal=True
            calle.save()

        calle_map = sen_calle_interseccion_dashboard_sensor.objects.get(scint_sensor=sensor).scint_calle_mapa.cmap_id
        calleMapa=geo_calle_mapa.objects.get(cmap_id=calle_map)
        calleMapa.cmap_calle=calle
        calleMapa.save()

        # PREGUNTAR SI LA INTERSECCION YA EXISTE
        banderaInterseccion=False
        for i in geo_interseccion_dashboard.objects.all():
            if((i.int_nombre).lower()==str(intersecciones).lower()):
                banderaInterseccion=True
                interseccion=geo_interseccion_dashboard(int_id=i.int_id)
                break

        if(banderaInterseccion==False):
            # Crea la interseccion/s
            interseccion=geo_interseccion_dashboard()
            interseccion.int_nombre=str(intersecciones).lower()
            interseccion.save()

        inter_map = sen_calle_interseccion_dashboard_sensor.objects.get(scint_sensor=sensor)
        inter_map.scint_intereseccion = interseccion
        inter_map.save()

        #sensor
        for s in sen_sensor_actuador.objects.exclude(sact_id=id):
            if ((s.sact_nombre).lower() == str(nombre).lower()):
                transaction.rollback()
                datos['message'] = "Existe un sensor con el mismo nombre"
                datos['result'] = "x"
                return HttpResponse(json.dumps(datos), content_type="application/json")
            if ((s.id_sensor).lower() == str(id_sensor).lower()):
                transaction.rollback()
                datos['message'] = "Existe un sensor con el mismo Id"
                datos['result'] = "x"
                return HttpResponse(json.dumps(datos), content_type="application/json")
        sensor=sen_sensor_actuador.objects.get(sact_id=id)

        sensor.sact_nombre=nombre
        sensor.dis_id_id = int(dispositivo)
        sensor.id_sensor = id_sensor
        sensor.sact_tipo = tipo

        #SEMAFORO
        semaforo = sen_semaforo.objects.get(sem_id=sensor)
        if(tipo=="semaforo"):
            semaforo.tipo='semaforo'
        else:
            semaforo.tipo='ultrasonico'
        semaforo.save()
        sensor.save()

        # Guarda en una lista las calles
        for c in geo_calle.objects.all():
            calles_nuevas.append(c.cal_nombre)

        # Guarda en una lista las intersecciones
        for i in geo_interseccion_dashboard.objects.all():
            intersecciones_nuevas.append(i.int_nombre)

        transaction.commit()
        calle_map = sen_calle_interseccion_dashboard_sensor.objects.get(scint_sensor=sensor).scint_calle_mapa.cmap_mapa.map_id
        datos['message'] = mensaje
        datos['lista_calles'] = calles_nuevas
        datos['lista_intersecciones'] = intersecciones_nuevas
        datos['map'] = calle_map
        datos['result'] = "OK"
    except IntegrityError as error:
        transaction.rollback()
        datos['message'] = "Error (Transaction):" + str(error)
        datos['result'] = "x"
    except Exception as error:
        transaction.rollback()
        datos['message'] = "Error:" + str(error)
        datos['result'] = "x"

    return HttpResponse(json.dumps(datos), content_type="application/json")