from django.conf.urls import include, url
from django.contrib import admin
from apps.geolocalizacion.views import *

urlpatterns = [
    url(r'^index', inicio, name="inicio_loc"),
    url(r'^eliminarUbicacion', eliminarMapa, name="urlEliminarMapa"),
    url(r'^eliminarTodo', eliminarTodo, name="urlEliminarTodo"),
    url(r'^intersecciones', cargar_intersecciones, name="cargar_intersecciones_select"),
    url(r'^verMarkers', verMarkers, name="urlEliminarTodo"),
    url(r'^agregarUbicacion', agregarUbicacion, name="agregarUbicacion"),
    url(r'^verDatosCalles', verDatosCalles, name="url_ver_calles"),
    url(r'^agregarCalles', agregarCalles, name="agregaCalles"),
    #CALLES
    url(r'^calles', calles, name="url_calles"),
    url(r'^verCalle', cargarCalles, name="cargarCalle"),
    url(r'^eliminarCalle', eliminarCalle, name="eliminarCalle"),
    url(r'^guardarCalle', guardarCalle, name="guardarCalle"),
     #INTERSECCIONES
    url(r'^eliminarInterseccion', eliminarInterseccion, name="eliminarInterseccion"),
    url(r'^verInterseccion', cargarIntersecciones, name="cargarIntersecciones"),
    url(r'^guardarInterseccion', guardarInterseccion, name="guardarInterseccion"),

    url(r'^verificarIdSensor', comprobarIDSensor, name="comprobarIDSensor"),
    url(r'^guardarDispositivoModal', guardarDispositivoAjax, name="guardarDispositivoAjax"),
    url(r'^cargarDispositivosSelect', cargarDispositivosSelect, name="cargarDispositivosSelect"),
    url(r'^guardarDependenciaModal', guardarDependenciaModal, name="guardarDependenciaModal"),

    url(r'^m/guardarCalleModal/', guardarCalleAjax, name="guardarCalleAjax"),
    url(r'^m/guardarInterseccionModal/', guardarInterseccionAjax, name="guardarInterseccionAjax"),
    url(r'^getMapa', cargar_mapa_modal, name="cargar_mapa_modal"),
    url(r'^getSensor', getSensor, name="getSensor"),
    url(r'^eliminarSensor', eliminarSensor, name="eliminarSensor"),
    url(r'^editarSensor', editarSensor, name="editarSensor"),


]