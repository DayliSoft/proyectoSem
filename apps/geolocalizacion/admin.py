from django.contrib import admin
from .models import *
admin.site.register(geo_mapa)
admin.site.register(geo_marker)
admin.site.register(geo_interseccion_dashboard)
admin.site.register(geo_calle)
admin.site.register(geo_calle_mapa)
