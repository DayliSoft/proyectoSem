
$().ready(function(){
   cargar_mapas();
   activarDiv();

});

function agregarInterseccion(){
    $("#modalLabelInterseccion").text("Agregar Intersección");
    if($("#btnGuardarYAddInterseccion").length == 0){
        parametros = "'txt_nombre_interseccion', 'formInterseccion', 'btnGuardarYAddInterseccion', '/maps/m/guardarInterseccionModal/', 'modalAgregarInterseccion'";
        $('<button id="btnGuardarYAddInterseccion" onclick="guardarYAddOtraCalleInter('+parametros+')" class="btn btn-primary" type="button"><i class="fa fa-check-square-o"></i> Guardar y agregar otra</button>').insertBefore("#btnGuardarInterseccion");
    }

    $('#modalAgregarInterseccion').modal({backdrop: "static", keyboard: false});
    document.getElementById("formInterseccion").reset();
}

function agregar(){
    $("#modalLabelCalle").text("Agregar Calle");
    if($("#btnGuardarYAddCalle").length == 0){
        parametros = "'txt_nombre_calle', 'formCalle', 'btnGuardarYAddCalle', '/maps/m/guardarCalleModal/', 'modalAgregarCalle'";
        $('<button id="btnGuardarYAddCalle" onclick="guardarYAddOtraCalleInter('+parametros+')" class="btn btn-primary" type="button"><i class="fa fa-check-square-o"></i> Guardar y agregar otra</button>').insertBefore("#btnGuardarCalle");
    }

    $('#modalAgregarCalle').modal({backdrop: "static", keyboard: false});
    document.getElementById("formCalle").reset();
}

function activarDiv(){
    $('#datatable-default_filter').find('input').addClass('form-control');
    $('#datatable-default1_filter').find('input').addClass('form-control');
    $('#dashboard').removeClass('nav-active');
    $('#dispositivo').removeClass('nav-active');
    $('#localidades').removeClass('nav-active');
    $('#dispositivo').removeClass('nav-active');
    $('#sensores').removeClass('nav-expanded');
    $('#sensores').removeClass('nav-active');
    $('#sensor').removeClass('nav-active');
    $('#mapa').removeClass('nav-active');

    $('#mapas').addClass('nav-expanded');
    $('#calle').addClass('nav-active');
}
function cargar_mapas(){
    $('#datatable-default').dataTable();
    $('#datatable-default1').dataTable();
}
function eliminar(id){
    $("#txt_id_calle").val(id);
    $('#modalEliminarCalle').modal({
        show: 'true'
    });

}
function eliminarInterseccion(id){
    $("#txt_id_interseccion").val(id);
    $('#modalEliminarInterseccion').modal({
        show: 'true'
    });

}

function verInterseccion(id){
    location.href="/portal/dashboard/"+id+"/";
}

function editarCalle(id){
    $.ajax({
        type: 'get',
        url: "/maps/verCalle/",
        data: {'pk':id},
        success: function(json){
            $('#txt_nombre_calle').val(json[0].nombre);
            $('#identificadorCalle').val(json[0].id);
            if(json[0].principal==true){
               $('#check').find('div.ios-switch').removeClass('off');
               $('#check').find('div.ios-switch').addClass('on');
           }else{
            $('#check').find('div.ios-switch').removeClass('on');
            $('#check').find('div.ios-switch').addClass('off');
            }
        }
    });
    $("#modalLabelCalle").text("Editar Calle");
    $("#btnGuardarYAddCalle").remove();
    $('#modalAgregarCalle').modal({backdrop: 'static', keyboard: false});
}

function editarInterseccion(id){
    $.ajax({
        type: 'get',
        url: "/maps/verInterseccion/",
        data: {'pk':id},
        success: function(json){
            $('#txt_nombre_interseccion').val(json[0].nombre)
            $('#identificadorInterseccion').val(json[0].id)
        }
    });
    $("#modalLabelInterseccion").text("Editar Intersección");
    $("#btnGuardarYAddInterseccion").remove();
    $('#modalAgregarInterseccion').modal({backdrop: 'static', keyboard: false});
}
