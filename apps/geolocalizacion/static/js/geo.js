
var mensaje="hola";
var latitud=0;
var longitud=0;
var ver;
var center;
var map = null;
var marker = null;
var markers = [];
var markersV = [];
var semaforo = '/static/assets/images/icons/traf.png';
var defaultI = '/static/assets/images/icons/home2.png';
var volumen = '/static/assets/images/icons/sound.png';
var id_mapa_cargar = "";
var id_sensor_cargar = "";

var infowindow;
$().ready(function(){
    localizame();
    cargar_categorias();
    cargar_selects();
    activarDiv();
});
function activarDiv(){
    $('#datatable-default_filter').find('input').addClass('form-control');
    $('#dashboard').removeClass('nav-active');
    $('#dispositivo').removeClass('nav-active');
    $('#localidades').removeClass('nav-active');
    $('#dispositivo').removeClass('nav-active');
    $('#sensores').removeClass('nav-expanded');
    $('#sensores').removeClass('nav-active');
    $('#sensor').removeClass('nav-active');
    $('#calle').removeClass('nav-active');

    $('#mapas').addClass('nav-expanded');
    $('#mapa').addClass('nav-active');
}

function cargar_selects(){
    $("#id_categoria_padre").select2();
    $.ajax({
        url:"/maps/intersecciones/",
        dataType: "json",
        async: true,
        success: function(json){
            $("#id_categoria_padre1").select2({
                placeholder: 'Otra (Definir nombre)',
                data: json,
                allowClear: true
            });
        }
    });

}

function eliminar(id){
    $('#modalEliminar').modal({
        show: 'true'
    });
    $("#idEliminar").val(id)
}
function eliminarObjeto(){
if(document.getElementById("idEliminar").value!='0'){
    $.ajax({
        type: 'get',
        url: "/maps/eliminarUbicacion/",
        data: {'idEliminar':document.getElementById("idEliminar").value},
        success: function(json){
            toastr.success(json.message, 'Correcto');
            $('#modalEliminar').modal('hide');
            $("#recargo").load(location.href+" #recargo>*","");
            $("#botones").load(location.href+" #botones>*","");

        }
    });
}else{
    eliminarTodo()
}
}
function eliminarTodo(){
    $.ajax({
        type: 'get',
        url: "/maps/eliminarTodo/",
        data: {'val':0},
        success: function(json){
            toastr.success(json.message, 'Correcto');
            $('#modalEliminar').modal('hide');
            $("#recargo").load(location.href+" #recargo>*","");
            $("#botones").load(location.href+" #botones>*","");
        }
    });
}


function agregarLocalidad(){
    var json_string = JSON.stringify( markersV );
    console.log(json_string);
    if($("#formulario_ubicacion").valid() == true && $("#id_categoria_padre_mapa").val() != "0") {
        var form = document.forms.namedItem("formulario_ubicacion");
        datos = new FormData(form);
        datos.append('latitud',marker.getPosition().lat());
        datos.append('longitud',marker.getPosition().lng());
        datos.append('marcadores',json_string);
        datos.append('id_mapa',id_mapa_cargar);

        $.ajax({
            type: "POST",
            url: '/maps/agregarUbicacion/',
            dataType: "json",
            async: true,
            data: datos,
            processData: false,
            contentType: false,
            success: function (json) {
                if (json.result == "OK") {
                    toastr.success(json.message, 'Correcto');

                    $('#MarkerModal').modal('hide');
                    $("#recargo").load(location.href+" #recargo>*","");
                    $("#recargo").load(location.href+" #recargo>*","");
                    $("#botones").load(location.href+" #botones>*","");

                    //Borrar markersV
                    markersV = [];
                    id_mapa_cargar = "";

                    //Actualizar selectizes si se agregaron nuevos elementos
                    var lista_calles = json.lista_calles;
                    if(lista_calles.length > 0){
                        var $select1 = $("#input-tags").selectize();
                        var calle = $select1[0].selectize;
                        calle.clearOptions();

                        for(var i = 0; i < lista_calles.length; i++){
                            calle.createItem(lista_calles[i]);
                            calle.refreshItems();
                        }
                    }

                    var lista_inters = json.lista_intersecciones;
                    if(lista_inters.length > 0){
                        var $select2 = $("#input-tags1").selectize();
                        var intersec = $select2[0].selectize;
                        intersec.clearOptions();

                        for(var j = 0; j < lista_inters.length; j++){
                            intersec.createItem(lista_inters[j]);
                            intersec.refreshItems();
                        }
                    }

                    //Cargar todos los iconos en el mapa
                    getMapaJson(json.id_mapa);
                } else {
                    toastr.error(json.message, 'Estado');
                }
            },
            error: function(e) {
                toastr.error("Ha ocurrido un error interno del servidor", 'Error');
                console.log(e)
            }
        });
    }else{
        toastr.warning("Revise que los campos ingresados sean válidos", 'Estado');
    }
}

function agregarCalle(){
    if($("#formularioCalles").valid() == true) {
        var form = document.forms.namedItem("formularioCalles");
        datos = new FormData(form);
        $.ajax({
            type: "POST",
            url: '/maps/agregarCalles/',
            dataType: "json",
            async: true,
            data: datos,
            processData: false,
            contentType: false,
            success: function (json) {
                if (json.result == "OK") {
                    toastr.success(json.message, 'Correcto');
                    $('#modalCalles').modal('hide');
                    cargar_selects();
                } else {
                    toastr.error(json.message, 'Estado');
                    $('#modalCalles').modal('hide');
                    cargar_selects();

                }
            },
            error: function(e) {
                console.log(e);
            }
        });
    }else{
        toastr.warning("Revise que los campos ingresados sean válidos", 'Estado');
    }


}

function editar(id){
    var inters;
    document.getElementById("formularioCalles").reset();
    var mapa=0;
    $('select').select2();
    $.ajax({
        type: 'get',
        url: "/maps/verDatosCalles/",
        data: {'pk':id},
        success: function(json){
            $('#ubicacion_nombre').val(json[0].text);
            $("#calle_nombre").val(json[0].nombre);
            $("#id_categoria_padre1").val(json[0].interseccion).trigger("change");
            if(json[0].principal==true){
               $('#check').find('div.ios-switch').removeClass('off');
               $('#check').find('div.ios-switch').addClass('on');
           }else{
            $('#check').find('div.ios-switch').removeClass('on');
            $('#check').find('div.ios-switch').addClass('off');

        }
    }
});
    $("#identificador").val(id);
    $('#modalCalles').modal({
        show: 'true'
    });
}


//////////////////////////////////
//////////MAPAS//////////////////
////////////////////////////////

function localizame() {
    if(id_mapa_cargar != "")
        markersV = [];

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(coordenadas, errores);
    }else{
        alert('Oops! Tu navegador no soporta geolocalización. Bájate Chrome, que es gratis!');
    }
}
function coordenadas(position) {
    latitud=position.coords.latitude; /*Guardamos nuestra latitud*/
    longitud=position.coords.longitude; /*Guardamos nuestra longitud*/
    dibujarMapa(latitud, longitud, 'Usted esta aqui', 'S/N', '', -1, 0);
}

function errores(err) {
    /*Controlamos los posibles errores */
    if (err.code == 0) {
      toastr.error("Oops! Algo ha salido mal",'Info')
  }
  if (err.code == 1) {
      toastr.warning("Oops! No has aceptado compartir tu posición","Info")
  }
  if (err.code == 2) {
      toastr.error("Oops! No se puede obtener la posición actual",'Info')
  }
  if (err.code == 3) {
      toastr.error("Oops! Hemos superado el tiempo de espera",'Info')
  }
  localizador();
  dibujarMapa(-1.91097, -78.684728, 'Usted está aqui', 'S/N', '', -1, 0);

}

function createInfoWindowContent(latLng, zoom, mapa, lugar, descripcion) {
  var scale = 1 << zoom;

  return [
  mapa,
  'Latitud y Longitud: ' + latLng,
  'Lugar: ' + lugar,
  '' + descripcion
  ].join('<br>');
}

function obtenerCoordenadas(marker1){
    // console.log(marker1.getPosition().lat())
    // console.log(marker1.getPosition().lng())

}

//AGREGAR TODOS LOS MARKERTS
        function agregarTodosMarkerts(id){

            // var neighborhoods = [];
            clearMarkers();
            markersV=[];
            $.ajax({
                url:"/maps/verMarkers/",
                dataType: "json",
                async: true,
                data: {'pk':id},
                success: function(json){
                    for (var i = 0; i < json.length; i++) {
                        addMarkerWithTimeout({'lat': json[i]['latitud'], 'lng': json[i]['longitud']}, i * 200,
                            json[i]['nombre'],json[i]['tipo'], json[i]['calle'], json[i]['interseccion'],json[i]['principal'], i, json[i]['id']);
                    }
                }
            });

        }
        function addMarkerWithTimeout(position, timeout, nombre, tipo, calle, interseccion, principal, i, id_sensor) {
        if(tipo=='semaforo'){
         markert = new google.maps.Marker({
            position: position,
                    // label: document.getElementById("txt_semaforo").value,
                    icon: semaforo,
                    animation: google.maps.Animation.DROP,
                    map: map
                });
        }else{
                   markert = new google.maps.Marker({
            position: position,
                    // label: document.getElementById("txt_semaforo").value,
                    icon: volumen,
                    animation: google.maps.Animation.DROP,
                    map: map
                });
        }

         console.log(markert.getPosition().lat());
         content = [
         nombre,
         'Latitud y Longitud: ' +markert.getPosition().lat()+' '+markert.getPosition().lng(),
         'Calles: '+ calle + ' entre ' + interseccion,
         'Tipo: '+ tipo
         ].join('<br>');
         var infowindow = new google.maps.InfoWindow({
            content: content
        });
         markert.addListener('click', function() {
             cargarDatosModalSensores(id_sensor);
            //infowindow.open(map, markert);
        });
         primero=[];
           //PRIMERO EL NOMBRE DEL SENSOR
          primero.push(nombre);
          //SEGUNDO LA CALLE Y SI ES PRINCIPAL
          primero.push(calle);
          primero.push(principal);
          //TERCERO LA INTERSECCION
          primero.push(interseccion);
          //TIPO DE SENSOR
          primero.push(tipo);
          primero.push(markert.getPosition().lat());
          primero.push(markert.getPosition().lng());
          //TIPO DISPOSITIVO
          primero.push(document.getElementById("select_dispositivo").value);
          //ID SENSOR
          primero.push(document.getElementById("txt_id_sensor").value.trim());

          markersV.push(primero)

     }
     function clearMarkers() {
      for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(null);
    }
    markers = [];
}
    //FIN DE TODOS LOS MARKERTS

function agregarSemaforo(){
    var comprobar = comprobarIdSensor();
    if(comprobar){
        lati=document.getElementById("longitud").value;
        longi=document.getElementById("latitud").value;
        // alert(document.getElementById("input-tags").value)

        marcadores = addMarker({'lat': parseFloat(lati), 'lng':parseFloat(longi)}, map);

        if(marcadores)
            $('#modalCalles1').modal('hide');
        else
            toastr.error('Los campos son obligatorios', 'Info');
    }
}

function comprobarIdSensor() {
    var bandera = false;
    var id_sensor = document.getElementById("txt_id_sensor").value.trim();
    var nombre = document.getElementById("txt_semaforo_nombre").value.trim();
    $('<img id="img_ajax_guardar" src="../../static/assets/images/ajax_load.gif">').insertBefore("#btnAgregarSemaforo");
    $.ajax({
        type: 'get',
        async: false,
        url: "/maps/verificarIdSensor/",
        data: {'id_sensor':id_sensor, 'nombre':nombre},
        success: function(json){
            $("#img_ajax_guardar").remove();
            if(json.result == "OK"){
                $('<input type="hidden" id="jsonResultSemaforo" />').insertBefore("#txt_id_sensor");
            }else{
                toastr.error(json.message, 'Error');
            }
        }
    });

    if($("#jsonResultSemaforo").length > 0){
        bandera = true;
        $("#jsonResultSemaforo").remove();
    }

    //Comprobar si ya existe el id_sensor o el nombre en el json markersV
    var mensaje = "";
    if(markersV.length > 0 && bandera == true) {
        for (var i = 0; i < markersV.length; i++) {
            if (markersV[i][8] == id_sensor) {
                bandera = false;
                mensaje = "El ID del sensor ya lo escribió hace unos momentos";
                break;
            }

            if (markersV[i][0] == nombre) {
                bandera = false;
                mensaje = "El nombre del sensor ya lo escribió hace unos momentos";
                break;
            }
        }
        if(bandera == false)
            toastr.error(mensaje, 'Error');
    }

    return bandera;
}


function addMarker(location, map) {
    elem1 = document.getElementById("txt_semaforo_nombre").value.trim();
    elem2 = document.getElementById("input-tags").value;
    elem3 = document.getElementById("input-tags1").value;
    elem4 = document.getElementById("select_dispositivo").value;
    elem5 = document.getElementById("txt_id_sensor").value.trim();

    if(elem1 == "" || elem2 == "" || elem3 == "" || elem4 == "0" || elem5 == ""){
        return false
    }else{
        tipo=document.getElementById("select_tipo").value;

        if(tipo=="ultrasonico"){
            var markert = new google.maps.Marker({
                position: location,
                icon: volumen,
                animation: google.maps.Animation.DROP,
                map: map
            });
        }else{
            var markert = new google.maps.Marker({
                position: location,
                // label: document.getElementById("txt_semaforo").value,
                icon: semaforo,
                animation: google.maps.Animation.DROP,
                map: map
            });
        }

        content = [
            document.getElementById("txt_semaforo_nombre").value,
            'Latitud y Longitud: ' +markert.getPosition().lat()+' '+markert.getPosition().lng(),
            'Calles: '+$("#input-tags").val()+ ' y '+$("#input-tags1").val()
        ].join('<br>');

        var infowindow = new google.maps.InfoWindow({
            content: content
        });

        markert.addListener('click', function() {
            infowindow.open(map, markert);
        });

        primero=[];
        //PRIMERO EL NOMBRE DEL SENSOR
        primero.push(elem1);
        //SEGUNDO LA CALLE Y SI ES PRINCIPAL
        primero.push(elem2);
        primero.push(document.getElementById("switch1").checked);
        //TERCERO LA INTERSECCION
        primero.push(elem3);
        //TIPO DE SENSOR
        primero.push(tipo);
        primero.push(markert.getPosition().lat());
        primero.push(markert.getPosition().lng());

        //TIPO DISPOSITIVO
        primero.push(elem4);
        //ID SENSOR
        primero.push(elem5);

        markersV.push(primero);

        return true;
    }
}

function toggleBounce() {
  if (marker.getAnimation() !== null) {
    marker.setAnimation(null);
} else {
    marker.setAnimation(google.maps.Animation.BOUNCE);
}
}



// FUNCION PARA DIBUJAR EL MAPA EN EL HTML
function dibujarMapa(lat, lng, mapa, lugar, descripcion, numerosMarcadores, id){
    id_mapa_cargar = id;
    var mapOptions = {
        center: new google.maps.LatLng(lat,lng),
        zoom: 18,
        zoomControl: true,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.DEFAULT
        },
        disableDoubleClickZoom: false,
        mapTypeControl: true,
        mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR
        },
        scaleControl: true,
        scrollwheel: true,
        panControl: true,
        streetViewControl: true,
        draggable : true,
        overviewMapControl: true,
        overviewMapControlOptions: {
            opened: false
        },
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    var mapElement = document.getElementById("gmap");
    map = new google.maps.Map(mapElement, mapOptions);

    google.maps.event.addListener(map, 'rightclick', function(event) {
        $("#longitud").val(event.latLng.lat());
        $("#latitud").val(event.latLng.lng());
        $("#btnEliminar").remove();
        $("#btnAgregarSemaforo").remove();
        $('<button type="button" id="btnAgregarSemaforo" onclick="agregarSemaforo()" class="btn btn-primary">Aceptar</button>').insertAfter("#btnHideSensor");

        //Reset campos
        $("#txt_id_sensor").val("");
        $("#txt_semaforo_nombre").val("");
        $("#txt_semaforo_nombre").focus();
        
        $("#gridSystemModalLabel").text("Agregar Sensor");
        $('#modalCalles1').modal({
            backdrop: 'static',
            keyboard: false
        });
    });
    // PARA DARLE CLIC AL MAPA
    // google.maps.event.addListener(map, 'click', function(event) {
    //     addMarker(event.latLng, map);
    // });
    var pos=new google.maps.LatLng(lat,lng);
    marker = new google.maps.Marker({
        position: pos,
        map: map,
        animation: google.maps.Animation.DROP,
        icon: defaultI,
        title:"Usted está Aquí",
        draggable: true
    });
    infowindow = new google.maps.InfoWindow;
    infowindow.setContent(createInfoWindowContent(pos, map.getZoom(), mapa, lugar, descripcion));
    marker.addListener('click', function() {
        infowindow.open(map, marker);
    });
    marker.addListener('click', toggleBounce);
    marker.addListener(marker, 'dragend', function() {
        obtenerCoordenadas(marker)
    });
    if(numerosMarcadores>=1){
        agregarTodosMarkerts(id);
    }

}

function abrirModalMarkers() {
    //Reset campos
    $("#txtDescripcion").val("");
    $("#txtNombre").val("");
    $("#txtNombre").focus();

    if(id_mapa_cargar != "") {
        $.getJSON("/maps/getMapa/?id="+id_mapa_cargar, function (data) {
            $("#txtNombre").val(data[0].nombre);
            $("#id_categoria_padre_mapa").val(data[0].localidades).trigger("change");
            $("#txtDescripcion").val(data[0].descripcion);
        });
    }

    $("#MarkerModal").modal({
        backdrop: 'static',
        keyboard: false
    })
}

function abrirModalAgregarDependencia() {
    cargar_objetos_select("select_dependencia_modal", "/localidades/ubi_select/");
    $("#txt_ubicacion_modal").val("");
    $("#gloModalDependencia").modal({
        backdrop: 'static',
        keyboard: false
    })
}

function cargarDatosModalSensores(id_sensor) {
    $.getJSON("/maps/getSensor/?id="+id_sensor, function (data) {
        console.log(data[0].principal);
            $("#txt_semaforo_nombre").val(data[0].nombre);
            $("#txt_id_sensor").val(data[0].sensor_id);
            var $select = $('#input-tags').selectize();
            var calles = $select[0].selectize;
            calles.addItem(data[0].calle);

            var $select2 = $('#input-tags1').selectize();
            var intersec = $select2[0].selectize;
            intersec.addItem(data[0].interseccion);

            if(data[0].principal==true){

               $('#check').find('div.ios-switch').removeClass('off');
               $('#check').find('div.ios-switch').addClass('on');
           }else{
                $('#check').find('div.ios-switch').removeClass('on');
                $('#check').find('div.ios-switch').addClass('off');

            }
            $("#select_dispositivo").val(data[0].dispositivo).trigger("change");
            $("#select_tipo").val(data[0].tipo).trigger("change");

            id_sensor_cargar = id_sensor;
            $("#btnEliminar").remove();
            $("#btnAgregarSemaforo").remove();
            $('<button type="button" id="btnAgregarSemaforo" onclick="editarSensorModal('+data[0].id+',1)" class="btn btn-primary">Editar</button>').insertAfter("#btnHideSensor");
            $('<button type="button" id="btnEliminar" onclick="eliminarSensor()" class="btn btn-danger">Eliminar</button>').insertBefore("#btnAgregarSemaforo");

            $("#gridSystemModalLabel").text("Editar Sensor");
             $("#modalCalles1").modal({
                backdrop: 'static',
                keyboard: false
            });
            console.log(markersV);
            console.log(primero);


        });
}

function eliminarSensor() {
    $.ajax({
        url:"/maps/eliminarSensor/?id="+id_sensor_cargar,
        dataType: "json",
        async: true,
        success: function(json){
            console.log(json);
            if(json.result == "OK"){
                getMapaJson(id_mapa_cargar);
                $("#modalCalles1").modal('hide');
                toastr.success(json.message, 'Estado');
            }else{
                toastr.error(json.message, 'Error');
            }
        }
    });
}

function getMapaJson(id_mapa) {
    $.getJSON("/maps/getMapa/?id="+id_mapa, function (data) {
        dibujarMapa(data[0].latitud, data[0].longitud,data[0].nombre, data[0].localidad, data[0].descripcion, data[0].contador, id_mapa)
    });
}


function cargarMapa(id) {
    getMapaJson(id)
}
