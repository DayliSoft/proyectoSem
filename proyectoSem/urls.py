"""proyectoSem URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url, patterns
from django.contrib import admin
from apps.users.views import *
from apps.portal.views import *
from views import validacion
from django.conf import settings

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^users/', include('apps.users.urls', namespace="users_app")),
    url(r'^portal/', include('apps.portal.urls', namespace="portal_app")),
    url(r'^localidades/', include('apps.localizacion.urls', namespace="locacalizacion_app")),
    url(r'^maps/', include('apps.geolocalizacion.urls', namespace="geolocalizacion_app")),
    url(r'^sensores/', include('apps.sensores.urls', namespace="sensores_app")),
    url(r'^$', pag_principal),
]
if settings.DEBUG:
    urlpatterns += patterns("",
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
            {'document_root': settings.MEDIA_ROOT,}
        ),
)
