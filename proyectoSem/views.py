from django.shortcuts import render
from django.contrib.auth import authenticate
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
# Create your views here.
def validacion(request):
    if not request.user.is_authenticated():
        return HttpResponseRedirect(reverse("users_app:login"))
    return HttpResponseRedirect(reverse('portal_app:index'))
