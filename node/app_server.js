const puerto = 8686;
const contadorConexiones = 0;
const rooms = []; // id de los paneles de monitoreo

const mqtt = require('mqtt');
const client  = mqtt.connect('mqtt://192.168.10.10');
const metodos = require('./funciones.js');
const io = require('socket.io')(puerto);

var deadline = new Date(Date.parse(new Date()) + 1 * 1 * 60 * 60 * 1000);

client.on('connect', function () {
  client.subscribe('lectura');
})

function allCalibracion(ids_intersecciones){
    var promise = new Promise((resolve,reject) => {
            for (var i = 0; i < ids_intersecciones.length; i++) {
                metodos.calibracion(ids_intersecciones[i])
                    .then(object => {
                        sendDataWSN(object.principal);
                            io.of('/conexiones').to('dashboard').emit('datos switch secundario', object.secundarios[i].id_semaforo);
                            for (var i = 0; i < object.secundarios.length; i++) {
                                metodos.setCalleSemaforoSecundario(object.secundarios[i])
                                .then(res => {
                                    if(res.result === 'OK'){
                                        console.log('Se actualizo exitosamente la calle el tiempo del semaforo SECUNDARIO exitosamente');
                                    }else{
                                        console.log('Hubo un error al mandar a modificar los datos de las calles y el semanforo SECUNDARIOS');
                                    }
                                })
                                .catch(err => console.log(err));
                            }
                    })
                    .catch(err => console.log(`Hubo un error: ${err}`))
            }
            resolve(true);
        });
    return promise;
}

function inicializarReloj(){

    // deadline = new Date(Date.parse(new Date()) + 1 * 1 * 60 * 60 * 1000);

    initializeClock(data => {
        inicializarReloj();
        metodos.getAllintersecciones()
            .then(data => allCalibracion(data))
            .then(objects => console.log(objects))
            .catch(err => console.log(err));
    });
}

function initializeClock(cb) {

  function updateClock() {

    var t = metodos.getTimeRemaining(deadline);

    var dias = t.days;
    var horas = ('0' + t.hours).slice(-2);
    var minutos = ('0' + t.minutes).slice(-2);
    var segundos = ('0' + t.seconds).slice(-2);

    io.of('/conexiones').to('reloj').emit('dia', dias);
    io.of('/conexiones').to('reloj').emit('hora', horas);
    io.of('/conexiones').to('reloj').emit('minutos', minutos);
    io.of('/conexiones').to('reloj').emit('segundos', segundos);

    if (t.total <= 0) {
      clearInterval(timeinterval);
      cb(true);
    }
  }

  updateClock();
  var timeinterval = setInterval(updateClock, 1000);
}

function sendDataWSN(object){
    console.log('Enviando principal a la WSN:');
    console.log(object);
    var promise = new Promise((resolve,reject) => {
        try {
            //client.publish('lectura', JSON.stringify(data));
            metodos.setCalleSemaforoPrincipal(object)
                .then(res => {
                    if(res.result === 'OK'){
                        io.of('/conexiones').to('dashboard').emit('datos switch principal', object.id_semaforo);
                        console.log('Se actualizo exitosamente la calle  el tiempo del semaforo PRINCIPAL exitosamente');
                    }else{
                        console.log('Hubo un error al mandar a modificar los datos de las calles y el semanforo PRINCIPAL');
                    }
                })
                .catch(err => console.log(err));
            resolve(true);
        }catch(err){
            reject(err);
        }
    });
    return promise;
}

client.on('message', (topico, msg) => {
    io.of('/conexiones').to('dashboard').emit('datos sensores', msg.toString());
});

io.of('/conexiones').on('connection', (socket) => {

    console.log("Nuevo cliente conectado. ");

    //Cuando se conecta el dashboard
    socket.on('subscribe', (room_id) => {

        socket.join(room_id);
        console.log(`Client join to room: ${room_id}`);
    });

    socket.on('calibracion', (id_interseccion) => {

        metodos.calibracion(id_interseccion)
            .then(object => {
                sendDataWSN(object.principal);
                for (var i = 0; i < object.secundarios.length; i++) {
                    io.of('/conexiones').to('dashboard').emit('datos switch secundario', object.secundarios[i].id_semaforo);
                    metodos.setCalleSemaforoSecundario(object.secundarios[i])
                        .then(res => {
                            if(res.result === 'OK'){
                                console.log('Se actualizo exitosamente la calle el tiempo del semaforo SECUNDARIO exitosamente');
                            }else{
                                console.log('Hubo un error al mandar a modificar los datos de las calles y el semanforo SECUNDARIOS');
                            }
                        })
                        .catch(err => console.log(err));
                        }
            })
            .then(resp => socket.emit('success', 'OK'))
            .catch(err => console.log(`Hubo un error: ${err}`))
    });

    socket.on('calibracion manual', (data) => {
        console.log('Esta llegando esto del cliente: ');
        console.log(data);
        for (var i = 0; i < data.length; i++) {
            if(data[i].calibracion){
                //client.publish('lectura', JSON.stringify(data[i]));
                metodos.analizarDataCalibracionManual(data[i])
                    .then(res => console.log(res))
                    .then(err => console.log(err));
            }
        }
    });

    socket.on('obtener end time', (data) => {
        if(data != 'OK'){
            deadline = data;
        }
    });

    //cuando se desconecta el dashboard
    socket.on('disconnect', (socket) => {

        console.log("Cliente desconectado del dashboard");
    });
});
console.log(`Servidor nodeJs corriendo en el puerto: ${puerto}`);
inicializarReloj();