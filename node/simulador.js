//var datos = require('./modelos/datos.js');
var tiempo = 1000;
var mac = ['F0:E1:D2:C3:B4:A5', 'F1:E2:D3:C4:B5:A6'];
var sensorSEm = ['semaforo1', 'semaforo2', 'ultrasonico3'];
var sensorUltr = ['ultrasonico1', 'ultrasonico2', 'semaforo3'];
var tipoSen = ['semaforo', 'ultrasonico']
var datoSem = ['V', 'R', 'A']

var mqtt = require('mqtt');
var client = mqtt.connect('mqtt://192.168.10.10');
client.on('connect', function () {
      client.subscribe('lectura')
      console.log('Suscrito al canal lectura');
})

function convertDate(inputFormat) {
    function pad(s) {
        return (s < 10) ? '0' + s : s;
    }
    var d = new Date(inputFormat);
    return [pad(d.getDate()), pad(d.getMonth() + 1), d.getFullYear()].join('/');
}

function simulador_lecturas() {

    valor = num_al(0, 1);
    var fecha = new Date();

    fecha = convertDate(fecha);
    var newDatos = null;
    if (valor == 0) {
        var num = num_al(0, 2);
        newDatos = {
            disp: mac[0],
            id_sensor: sensorSEm[num],
            tipo: tipoSen[0],
            dato: datoSem[num_al(0, 2)]
        };
        if(num===2){
            newDatos.tipo = tipoSen[1];
            newDatos.dato = num_al(0, 100);
        }
    } else {
        var num = num_al(0, 2);
        newDatos = {
            disp: mac[num_al(0, 1)],
            id_sensor: sensorUltr[num],
            tipo: tipoSen[1],
            dato:  num_al(0, 100)
        };
        if(num===2){
            newDatos.tipo = tipoSen[0];
            newDatos.dato = datoSem[num_al(0, 2)];
        }
    }
    envio_datos_mqtt(newDatos);
}

function num_al(desde, hasta) {
    var medida = Math.floor((Math.random() * ((hasta + 1) - desde))) + desde;
    return medida;
}

function envio_datos_mqtt(data){
    console.log(`Enviando datos ${JSON.stringify(data)}`);
    client.publish('lectura', JSON.stringify(data));
}

//Inicio del simulador
setInterval(function() {
    simulador_lecturas();
}, tiempo);
