const datos_postgres = require('./modelos/datos_postgres.js');
const datos_mongo = require('./modelos/datos.js');
const com_django = require('./comunicacion_django.js');

function semaforoPrincipal(sensor_id){

    var promise = new Promise((resolve,reject) => {
        datos_mongo.count({id_sensor: sensor_id, tipo: 'ultrasonico'}, function( err, count){
            if (err) reject(err);
            resolve(count);
        });
    });
    return promise;
}

function analizarDatos(array){

    var promise = new Promise((resolve,reject) => {
        try {
            var respuesta = null;
            var indice = 0;
            for (var i = 0; i < array.length; i++) {
                for (var j = 0; j < array.length; j++) {
                    if(array[i].cant >= array[j].cant){
                        respuesta = array[i];
                        indice = i;
                    }else{
                        break;
                    }
                }
            }
            //console.log(`El mayor es: ${respuesta.id_sensor} con la cantidad de ${respuesta.cant} circulados`);
            array.splice(indice,1);
            dic = {
                'principal': respuesta,
                'secundarios' : array
            }
            resolve(dic);
        }catch(err){
            reject(err);
        }
    });
    return promise;
}

function delayLoop(sensoresArray,con,callback) {
    // console.log(`if (${con} < ${sensoresArray.length})`);
    if (con < sensoresArray.length) {

        semaforoPrincipal(sensoresArray[con].id_sensor)
            .then(cantidad => {
                sensoresArray[con].cant = cantidad;
                con++;
                delayLoop(sensoresArray,con,msg => {
                    callback(msg);
                });
            })
            .catch(err => {
                console.log('HUBO UN ERROR: '+ err);
            })

    } else {
        con = 0;
        callback('Se termino la ejecucion del bucle de callbacks');
    }
}

function calibracion(id_interseccion){

    var promise = new Promise((resolve,reject) => {
        var sensoresArray=[];
        var link = '/portal/datos_interseccion';
        var dic = {
            id_interseccion: id_interseccion
        }
        com_django.envio_datos(dic, link, (data) => {
            if(data.result === 'OK'){

                sensoresArray = data.data;
                delayLoop(sensoresArray,0,msg => {
                    analizarDatos(sensoresArray)
                        .then(object => resolve(object))
                        .catch(err => reject(err));
                })
            }
        });
    });
    return promise;
}


function getAllintersecciones(){

    var promise = new Promise((resolve,reject) => {
        var link = '/portal/getAllintersecciones';
        var dic = {
            id_interseccion: 'all'
        }
        com_django.envio_datos(dic, link, (data) => {
            if(data.result === 'OK'){
                resolve(data.data);
            }
        });
    });
    return promise;
}

function setCalleSemaforoPrincipal(object){
    var promise = new Promise((resolve,reject) => {
        try {
            var link = '/portal/setCalleSemaforoPrincipal';
            com_django.envio_datos(object, link, (data) => {
                resolve(data)
            });
        }catch(err){
            reject(err);
        }
    });
    return promise;
}

function setCalleSemaforoSecundario(object){
    var promise = new Promise((resolve,reject) => {
        try {
            var link = '/portal/setCalleSemaforoSecundario';
            com_django.envio_datos(object, link, (data) => {
                resolve(data)
            });
        }catch(err){
            reject(err);
        }
    });
    return promise;
}

function getTimeRemaining(endtime) {
  var t = Date.parse(endtime) - Date.parse(new Date());
  var seconds = Math.floor((t / 1000) % 60);
  var minutes = Math.floor((t / 1000 / 60) % 60);
  var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
  var days = Math.floor(t / (1000 * 60 * 60 * 24));
  return {
    'total': t,
    'days': days,
    'hours': hours,
    'minutes': minutes,
    'seconds': seconds
  };
}

function analizarDataCalibracionManual(object){

    var promise = new Promise((resolve,reject) => {
        try {
            if(object.estado){
                var link = '/portal/setCalleSemaforoPrincipal';
            }else{
                var link = '/portal/setCalleSemaforoSecundario';
            }
            com_django.envio_datos(object, link, (data) => {
                resolve(data)
            });
        }catch(err){
            reject(err);
        }
    });
    return promise;
}

module.exports.semaforoPrincipal = semaforoPrincipal;
module.exports.analizarDatos = analizarDatos;
module.exports.calibracion = calibracion;
module.exports.delayLoop = delayLoop;
module.exports.getTimeRemaining = getTimeRemaining;
module.exports.getAllintersecciones = getAllintersecciones;
module.exports.setCalleSemaforoPrincipal = setCalleSemaforoPrincipal;
module.exports.setCalleSemaforoSecundario = setCalleSemaforoSecundario;
module.exports.analizarDataCalibracionManual = analizarDataCalibracionManual;