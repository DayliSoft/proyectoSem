//https://fernetjs.com/2011/12/creando-y-utilizando-callbacks/
//http://stackoverflow.com/questions/19322248/node-js-distinguishing-errors-when-making-http-request
var http = require('http');
var querystring = require('querystring');

function envio_datos(data, link, callback) {
    var values = querystring.stringify(data);
    var options = {
        hostname: 'localhost',
        port: '8000',
        path: link,
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-Length': values.length,
        }
    };

    var req = http.request(options, function(res) {

        res.setEncoding('utf8');
        res.on('data', function(data) {

            try {
                data = JSON.parse(data);
                callback(data);
            } catch (e) {
                console.log(e);
            }
        });
    });

    req.on('error', function(e) {

        console.log('No se esta conectado el servidor de node a Django:');
    });

    req.on('timeout', function() {

        console.log('Se demoro el servidor en responder.');
        req.abort();
    });

    req.setTimeout(10000);
    req.write(values);
    req.end();
}

module.exports.envio_datos = envio_datos;

