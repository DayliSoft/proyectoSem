var mongoose = require("mongoose");
var Schema = mongoose.Schema;

mongoose.connect("mongodb://localhost/semaforo", function(err) {
    if (err) {
        console.error('Error de conexion: no esta inicializado el servidor de mongoDB');
    }
});

var datos = new Schema ({
	disp: String,//mac del dispositivo (Arduino)
	id_sensor: String, //id del sensor o algun identificativo unico
	tipo: String,//tipo si es sensor ultrasonico o del semaforo
	fecha: String,//fecha de la lectura
	dato : String,// 1 ->	z si es carro que paso o ( v=verde, a=anaranjado, r=rojo) en caso del semafoto
});

module.exports = mongoose.model('datos', datos);
