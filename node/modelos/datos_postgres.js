var Sequelize = require('sequelize');
var sequelize = new Sequelize('semaforo1', 'postgres', 'admin', {
    host: '127.0.0.1',
    dialect: 'postgres',
      // disable logging; default: console.log
  	logging: false,

    pool: {
        max: 5,
        min: 0,
        idle: 10000
    },

});

var Sensor_actuador = sequelize.define('sensores_sen_sensor_actuador', {
    sact_id: {
        type: Sequelize.INTEGER,
        primaryKey: true
    },
    sact_tipo: Sequelize.STRING,
    sact_nombre: Sequelize.STRING,
    dis_id_id: Sequelize.INTEGER,
    sact_latitud: Sequelize.FLOAT,
    sact_longitud: Sequelize.FLOAT,
}, {
    freezeTableName: true,
	timestamps: false,
});

var Sensor_calle_interseccion = sequelize.define('sensores_sen_calle_interseccion_dashboard_sensor', {
    scint_id: {
    	type: Sequelize.INTEGER,
    	primaryKey: true
    },
    scint_intereseccion_id: Sequelize.INTEGER,
    scint_sensor_id: {
        type: Sequelize.INTEGER,
        referencies: {
            model: Sensor_actuador,
            key: 'sact_id',
            deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
        }
    },
    scint_calle_mapa_id: Sequelize.INTEGER,
},{
    freezeTableName: true,
	timestamps: false,
});

Sensor_actuador.hasMany(Sensor_calle_interseccion,{
    foreignKey: 'scint_sensor_id'
})

module.exports.sequelize = sequelize;
module.exports.sensor_actuador = Sensor_actuador;
module.exports.sensor_calle_interseccion = Sensor_calle_interseccion;