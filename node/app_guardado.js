const mqtt = require('mqtt')
const datos = require('./modelos/datos.js')
const client  = mqtt.connect('mqtt://192.168.10.10')

client.on('connect', () => {
    client.subscribe('lectura');
    console.log('Conectado');
})

client.on('message', (topic, message) => {
    var data = JSON.parse(message.toString());
    // guardar datos a mongo
    guardadoDatos(data,(msg) => {
        console.log(msg);
    });
})

function guardadoDatos(data, cb){

    var fecha = new Date();
    fecha = convertDate(fecha);

    var newDatos = datos();
    newDatos.disp = data.disp;
    newDatos.id_sensor = data.id_sensor;
    newDatos.tipo =data.tipo;
    newDatos.fecha = fecha;
    newDatos.dato = data.dato;
    newDatos.save(function(err) {
        if (err) throw err;
        cb("Lectura sensor guardada");
    });
}

function convertDate(inputFormat) {
    function pad(s) {
        return (s < 10) ? '0' + s : s;
    }
    var d = new Date(inputFormat);
    return [pad(d.getDate()), pad(d.getMonth() + 1), d.getFullYear()].join('/');
}

console.log(`Microservicio: Guardado y analisis de datos -> activado`);